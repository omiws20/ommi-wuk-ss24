# Aufgaben zu affine Chiffren

Die Aufgaben zu den affinen Chiffren sind immer gleich aufgebaut. Es geht ledgiglich um zwei Details:

1. Die bekannten Übergänge (z.B. E->Y)

2. Aus den Übergängen werden die linearen Gleichungen abgeleitet. Pro Gleichung gibt es eine Variable, die bestimmt werden muss.

In der Klausur werden es höchstwahrscheinlich nur zwei Gleichungen sein. Damit können beide Gleichungen einfach aufgelöst werden.

## Aufgabe 1 (Dozent/SS24)

f(x) = a x + b mod 26

ABCD...Z -> 0,1,...25

Sprachstatistik

E -> Y

N -> K

Demnach ist E = 4, Y = 24, N = 13 und K = 10.

A-0, B-1, C-2, D-3, E-4, F-5, G-6, H-7, I-8, J-9, K-10, L-11, M-12, N-13, O-14, P-15, Q-16, R-17, S-18, T-19, U-20, V-21, W-22, X-23, Y-24, Z-25

**Tipp:** ChatGPT fragen;)

Damit ergeben sich **zwei** Modulo 26-Gleichungssysteme:

f(4)  = 24 a + b mod 26

f(13) = 10 a + b mod 26

Einsetzen in die Funktion

I   4a + b = 24 mod 26

II 13a + b = 10 mod 26

Auflösen des LSG, Multiplikation mit des inversen Modulo, **nicht** erlaubt ist eine Division!

I-II 9a = -14 mod 26

III  9a = 12 mod 26

Um nach a auflösen zu können, muss die Inverse von 9 mod 26 bestimmt werden.

Gesucht ist ein b, das die Gleichung 9 b ≡ 1 (mod 26) erfüllt.

Voraussetzung für die Anwendung des erweiterten Euklid ist, dass der ggT(9, 26) = 1 ist.

26 = 2 * 9 + 8

 9 = 1 * 8 + 1

Damit ist erneut "bewiesen", dass der ggT(9, 26) = 1 ist.

Jetzt alles rückwärts:

1 = 9 - 1 * 8

Auch wenn es verlockend sein könnte, bitte die Gleichung nicht auflösen, da es dann keine lineare Gleichung mehr wäre, aus der sich der Wert der modulare Inversen ablesen ließe (dafür gibt es dann keine Punkte;)

Mit 8 = 26 - 2 * 9 ergibt sich

1 = 9 - 1 * (26 - 2 * 9)

  = 9 - 26 + 2 * 9

  = **3** * 9 - 26

Der modulo Invers ist damit 3.

Zurück zur Gleichung

9a = 12 mod 26

Diese wird mit 3 multipliziert

3 * 9a = 3 * 12 mod 26

Aus 3 * 9a kann a gemacht werden, da 3 × 9 ≡ 1 (mod26):

a = 36 mod 26

a = 10

a einsetzen in LGS, z.B. in I

-> b = 24 mod 26 - 40 = -16

Addition von 26 -> b = 10

Da 9 und 26 teilerfremd sind, gibt es keine weitere Lösungen.

Eine Frage in der Klausur war, warum es für a mehrere  (zwei) Lösungen geben kann.

Die Antwort ist wahrscheinlich (mangels mathematischer Detailkenntnisse) umfangreicher als sie sein müsste.

Auf der Suche nach der modulo Inversen kann es für die Gleichung

a * a hoch -1 ≡ 1 mod m

zwei Werte für a geben, die zum selben Resultat führen. Das hat mit der "zyklischen" Natur der Gleichung zu tun.
 
Beispiel:

Für m = 26 kommen für a sowohl die Werte 5 und 21 in Frage:

5 * 21 = 105 = 1 mod 26

Sowohl 5 als auch 21 erfüllen die Bedigung für einen Wert für a, damit ein inverser Modulo resultiert.

## Aufgabe 2 (Klausur SS 2014)

f(x) = a x + b mod 26

ABCD...Z -> 0,1,...25

Sprachstatistik

E -> O
O -> W

### Lösung

E -> O => f(4) = 14
O -> W => f(14) = 22

Damit ergeben sich zwei Modulo 26-Gleichungssysteme:

(I)   4a + b  = 14 (mod 26)
(II) 14a + b  = 22 (mod 26)

Substraktion I - II:

10 a = 8 mod 26

**Wichtig**: 26 und 10 sind nicht teilerfremd, der ggT(26,10) = 2. Es kann aber trotzdem (bitte ChatGPT fragen) ein inverses Modulo gebildet werden und am Ende kommt für a auch 6 heraus. Der Rechenweg folgt am Ende.

Im Moment verwende ich Ausprobieren was bei der 6 noch relativ einfach ist. Damit ergeben sich zwei Lösungen:

a1 = 6

a2 = 19

Einsetzen von a=6 in I:

24 + b = 14 (mod 26)

b = -10 (mod 26)

b = 16 (mod 26)

b = 16

Einsetzen von a=19 in I:

76 + b = 14 (mod 26)

b = -62 (mod 26)

b = 16 (mod 26)

b = 16

Berechnung per ChatGPT:

**Schritt 1**: Berechnen des ggT(10, 26) per erweiterten Euklid:

26 = 2 * 10 + 6
10 = 1 * 6 + 4
 6 = 1 * 4 + 2
 4 = 2 * 2 + 0

Der ggT(26,10) ist damit 2.

Es wird aber nicht rückwärts gerechnet (wahrscheinlich, da der ggT nicht 1 ist).

Stattdessen wird die Gleichung

10 a ≡ 8 mod 26

durch den ggT geteilt:

5 a ≡ 4 mod 13

Jetzt wird es interessant. Da ggT(5, 13) = 1 ist, kann jetzt der erweiterte Euklid angewendet werden:

13 = 2 * 5 + 3

 5 = 1 * 3 + 2

 3 = 1 * 2 + 1

Jetzt aber endlich rückwärts:

1 = 3 - 1 * 2

mit 2 = 5 - 1 * 3 => 

1 = 3 - 1 * (5 - 1 * 3)

  = 2 * 3 - 1 * 5

Jetzt muss die 13 reingebracht werden:

  = 2 × (13 − 2 × 5) − 1 × 5

  = 2 * 13 - 5  * 5

Der modulare Inverse von 5 modulo 13 ist damit -5 bzw. 8 (+ 13, da es keine negative Zahl sein soll).

In der Gleichung

5a ≡ 4 mod 13

werden beide Seiten mit 8 multipliziert:

40a = 32 mod 13

bzw.

a = 32 mod 13 = 6

Damit steht, nach viel Rechnerei, das Ergebnis fest: a = 6