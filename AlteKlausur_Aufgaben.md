# Klausuraufgaben

## Aufgabe 1 (SS 2014)


## Aufgabe 2 (SS 2014)

Es ist immer diesselbe Aufgabenstellung, daher nur der Form halber

f(x) = a x + b mod 26

ABCD...Z -> 0,1,...25

** Ab hier wird es interessant:**

Eine Sprachstatistik ergab:

E -> O
O -> W

### Lösung

**Schritt 1:** Aufstellen der Gleichungen

E -> O => f(4) = 14
O -> W => f(14) = 22

Damit ergeben sich zwei Modulo 26-Gleichungssysteme:

(I)   4a + b  = 14 (mod 26)
(II) 14a + b  = 22 (mod 26)

**Schritt 2:** Auflösen nach a

Substraktion I - II:

10 a = 8 mod 26

Es gibt kein Inverse Modulo, da 10 und 26 nicht teilerfremd sind, d.h. ggT(10,26) = 2 und nicht 1.

a muss daher durch Ausprobieren gefunden werden, da die Berechnung eines Inverse modulo 26 über den erweiterten Euklid nicht möglich ist.

Es ergeben sich zwei Lösungen für a.

a1 = 6
a2 = 19

(Es ergeben sich immmer zwei Lösungen).

**Schritt 3:** Einsetzen von a, um b zu erhalten

Einsetzen von a=6 in I:

24 + b = 14 (mod 26)
b = -10 (mod 26)       # Addition von 26, damit die Zahl positiv wird
b = 16 (mod 26)
b = 16

Einsetzen von a=19 in I:

76 + b = 14 (mod 26)
b = -62 (mod 26)       # Addition von 3x26=78
b = 16 (mod 26)
b = 16

Die Lösung ist a=6 oder 19 und b=16.

## Aufgabe 3 (SS 2014)


## Aufgabe 4 (SS 2014)


## Aufgabe 5 (SS 2014)


## Aufgabe 6 (SS 2014)


## Aufgabe 7 (SS 2014)


## Aufgabe 8 (SS 2014)


