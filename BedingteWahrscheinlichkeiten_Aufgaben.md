# Bedingte Wahrscheinlichkeiten-Aufgaben

## Aufgabe 1 (Dozent/SS24)

Ein Unternehmen produziert ein Produkt an zwei Standorten A (Anteil 70%) und B (Anteil 30%).

Am Standort A ist die W. eines Defekts 10%

Am Standort B ist die W. eines Defekts 20%

Wie groß ist die Wahrscheinlichkeit, dass ein defektes Produkt von Standort A stammt?

### Lösung

Gesucht ist 

PA(D) = P(D | A) - Wahrscheinlichkeit, dass D in Abhängigkeit von A eintritt

**Schritt 1**: Zeichen eines Ereignisbaumes mit P(A) = 0.7 und P(B) = 0.3

*** Fehlt noch

Aus dem Baum lässt sich ablesen:

P(A ∩ D) = P(A) * PA(D) = 0.7 * 0.1 = 0.07

Die Wahrscheinlichkeit, dass ein Produkt defekt ist und es von Standort A stammt beträgt 7%.

**Schritt 2**: Berechnen der vollständigen Wahrscheinlichkeit D

Im nächsten Schritt wird die vollständige Wahrscheinlichkeit für ein defektes Produkt berechnet.

P(D) = P(A ∩ D) + P(̅A ∩ D)
     = 0.7 * 0.1 + 0.3 * 0.2
     = 0.13

Die Wahrscheinlichkeit für ein defektes Produkt beträgt 13%.

**Schritt 3**: Umgekehrter Ereignisbaum zur Kontrolle (optional)

*** Fehlt noch

Aus der Pfadregel folgt

P(A ∩ D) = P(A) * PA(D)

**Schritt 4**: Satz von Bayes

PD(A) * P(D) = P(A) * PA(D)

PD(A) = P(A) * PA(D) / P(D)
      = P(A) * PA(D) / P(A ∩ D)  + P(̅A ∩ D)

      = 0.7 * 0.1 / 0.13 = 0.538

Die Wahrscheinlichkeit P(A|D), dass ein defektes Produkt aus Werk A stammt, beträgt 53.8%.

## Aufgabe 2 (Satz von Bayes)

Ereignis A: Eine Person hat eine Krankheit K
Ereignis B: Das Testergebnis für die Krankheit ist positiv

Gesucht ist die Wahrscheinlichkeit dafür, dass die Person die Krankheit auch tatsächlich hat.

Die vorgegebenen Wahrscheinlichkeiten sind:

P(A) = 0.01
P(̅A) = 0.99
P(B|A) = 0.99
P(B|̅A)= 0.05

Gesucht ist P(A|B), d.h. die Wahrscheinlichkeit, dass die Person die Krankheit hat wenn der Test positiv ist.

### Lösung

**Schritt 1:** Berechnen der totalen Wahrscheinlichkeit

P(B) = Wahrscheinlichkeit für eine Erkrankung

P(P) = P(B|A) * P(A) + P(B|̅A) * P(A)
     = 0.99 * 0.01 + 0.05 * 0.99
     = 0.0594

**Schritt 2:** Satz von Bayes

P(A|B) = P(B|A) * P(A) / P(B)
       = 0.99 * 0.01 / 0.0594 
       = 0.1667 ≅ 16.67%

Die Wahrscheinlichkeit für eine Erkrankung bei einem positiven Testergebnis beträgt "nur" 16.67%.

## Aufgabe 3 (Totale Wahrscheinlichkeit, Klausuraufgabe SS23)

Für die Mitarbeiter einer Firma stehen drei Verkehrsmittel zur Verfügung:

1. Auto
2. Fahrrad
3. ÖPVN

Jede dieser Verkehrsmittel ist mit einer bestimmten Wahrscheinlichkeit unpünktlich:

P(A) = 0.5
P(F) = 0.2
P(Ö) = 0.3

Wie groß ist die Wahrscheinlichkeit, dass ein Mitarbeiter M pünktlich zur Arbeit kommt?

### Lösung

Gesucht ist die Wahrscheinlichkeit P(P).

*** Wo kommen die Wahrscheinlichkeiten her? Berechnet oder Teil der Aufgabe?

P(P|A) = 0.7
P(P|F) = 0.9
P(P|Ö) = 0.8

Berechnen der totalen Wahrscheinlichkeit

P(P) = P(P|A) * P(A) + P(P|F) * P(F) + P(P|Ö) * P(Ö)
     = 0.7 * 0.5 + 0.2 * 0.9 + 0.3 * 0.8
     = 0.77 = 77%

Die Wahrscheinlichkeit, dass ein Mitarbeiter unabhängig von der Wahl des Verkehrsmittels pünktlich zur Arbeit kommt beträgt demnach 77%.

## Aufgabe 3 (Satz von Bayes)

Ereignis A: Das Testergebnis für Brustkrebs ist positiv
Ereignis B: Die Patientin hat Brustkrebs

Folgende Wahrscheinlichkeiten sind gegeben:

P(Brustkrebs) = 0.01
P(Test positiv) = 0.8, wenn Krankheit vorhanden, ansonsten 10%

### Lösung

### Schritt 1: Aufzeichnen eines Baumdiagramms

*** fehlt noch

Daraus ergeben sich die bedingten Wahrscheinlichkeiten

Gesucht ist P(B|Test positiv)

### Schritt 2: Anwenden des Satz von Bayes

*** alternativ 4-Felder-Tafel

Aus dem Baumdiagramm lässt sich ablesen:

P(Test positiv|Brustkrebs) = 0.01 * 0.8 = 0.008
P(Test nicht positiv|Brustkrebs) = 0.01 * 0.12 = 0.0012
P(Test positiv|kein Brustkrebs) = 0.99 * 0.1 = 0.099
P(Test nicht positiv|kein Brustkrebs) = 0.99 * 0.9 = 0.891

*Satz von Bayes*:

P(P) ist die Komplettwahrscheinlichkeit für ein positives Ergebnis und wird aus dem Baum abgelesen.

P(P) = 0.01 * 0.8 + 0.99 * 0.1 = 0.107

Pp(B) = P(P ∩ B) / P(P) = 0.008 / 0.107 = 0.0747 ≈ 7.4%

Die Wahrscheinlichkeit für die Erkrankung an Brustkrebs bei einem positiven Testergebnis beträgt "nur" 7.47%.

## Aufgabe 3 (Satz von Bayes, Klausur SS 2014)

Eine seltene Krankheit tritt mit einer Wahrscheinlichkeit von 10 hoch -5 auf. Ein Antikörpertest für diese Krankheit erkennt eine Infektion mit einer Wahrscheinlichkeit von 95%.

Gesunde Probanden werden mit einer Wahrscheinlichkeit von 0.5% als irrtümlich krank eingestuft (positives Testergebnis).

Wie groß ist die Wahrscheinlichkeit, dass bei einem positiven Testergebnis tatsächlich eine Erkrankung vorliegt?

### Lösung

**Schritt 1**: Definieren der beiden Ereignisse

Ereignis K: Der Proband ist krank
Ereignis T: Der Proband hat ein positives Testergebnis

**Schritt 2**: Aufschreiben der gegebenenen Werte

P(K) = 10 hoch -5
P(T|K) = 0.95
P(T|̅K) = 0.05 
=> P(̅K) = 1 - 10 hoch -5

Gesucht ist P(K|T)

**Schritt 3**: Satz von Bayes anwenden

P(K|T) = P(T|K) * P(K) / P(T|K) * P(K) + P(T|̅K) * P(̅K) = 0.95 * 10 hoch -5 / 0.95 * 10 hoch -5 + (1 - 10 hoch -5)
       = ??? = 1.9% (laut Musterlösung)

*** ich habe mit dem taschenrechner eine halbe Stunde gebraucht, um das auszurechnen und es hat am Ende immer noch nicht gestimmt

ChatGPT sagt: 0.0019 bzw. 0.19%



