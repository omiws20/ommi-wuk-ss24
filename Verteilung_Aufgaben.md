# Aufgaben zu Verteilungen

## Glühbirnen

### Aufgabe 1 (Binomialverteilung)

#### Aufgabe

Ein Unternehmen produziert elektronische Bauteile. Es ist bekannt, dass 95 % der produzierten Bauteile fehlerfrei sind. Die restlichen 5 % der Bauteile haben einen Fehler.

Eine Qualitätskontrolle wird durchgeführt, bei der zufällig 20 Bauteile ausgewählt und getestet werden.

A. Wie hoch ist die Wahrscheinlichkeit, dass genau 18 der 20 getesteten Bauteile fehlerfrei sind?

B. Wie hoch ist die Wahrscheinlichkeit, dass mindestens 19 der 20 getesteten Bauteile fehlerfrei sind?

#### Lösung A

18 von 20 Bauteilen sind fehlerfrei

Anwenden der Binomialverteilung mit

n = 20 (Anzahl der Versuche)
k = 18 (Anzahl der Erfolge)
p = 0.95 (Wahrscheinlichkeit eines Erfolgs)

P(X=18) = (20 über 18) * 0.95 hoch 18 * 0.05 hoch 2

= ???

#### Lösung B

Mindestens 19 fehlerfreie Bauteile

Das umfasst die Wahrscheinlichkeiten für 19 und 20 fehlerfreie Bauteile

P(X≥19) = P(X=19) + P(X=20)

Die Wahrscheinlichkeiten für X=19 und X=20 werden separat berechnet:

P(X=19) = (20 über 19) * 0.95 hoch 19 * 0.05 hoch 1
= ???

P(X=20) = (20 über 20) * 0.95 hoch 20 * 0.05 hoch 0
= ???

Beide Wahrscheinlichkeiten werden addiert:

P(X≥19) = ??? + ???

= ???

### Aufgabe 2 (Hypergeometrische Verteilung)

#### Aufgabe

In einem Lager befinden sich insgesamt 1000 Glühbirnen. Davon sind 200 defekt und 800 funktionstüchtig. Ein Qualitätsprüfer entnimmt zufällig eine Stichprobe von 50 Glühbirnen ohne Zurücklegen.

A. Wie hoch ist die Wahrscheinlichkeit, dass genau 5 der 50 entnommenen Glühbirnen defekt sind?

B. Wie hoch ist die Wahrscheinlichkeit, dass höchstens 3 der 50 entnommenen Glühbirnen defekt sind?

#### Lösung A

Genau 5 defekte Glühbirnen

Anwenden der Formel zur hypergeometrischen Verteilung mit

N = 1000 (Gesamtzahl der Glühbirnen)
K = 200 (Gesamtzahl der defekten Glühbirnen)
n = 50 (Anzahl der gezogenen Glühbirnen)
k = 5 (Anzahl der defekten Glühbirnen in der Stichprobe)

P(X=5) = (200 über 5) * (800 über 45) / (1000 über 50)

= ???

#### Lösung B

Höchstens 3 defekte Glühbirnen

Das umfasst die Wahrscheinlichkeiten für 0, 1, 2 und 3 defekte Glühbirnen

Berechnen der Wahrscheinlichkeiten für X=0, X=1, X=2 und X=3:

P(X=0) = (200 über 0) * (800 über 50) / (1000 über 50)

P(X=1) = (199 über 1) * (800 über 49) / (1000 über 50)

P(X=2) = (200 über 2) * (800 über 48) / (1000 über 50)

P(X=3) = (200 über 3) * (800 über 47) / (1000 über 50)

P(X≤3) = P(X=0) + P(X=1) + P(X=2) + P(X=3)

= ???



