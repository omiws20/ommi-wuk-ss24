# Aufgaben zu Modulo-Operationen

## Aufgabe 1 (Klausur SS 2014)
Berechne per Hand möglichst effizient 5 hoch 292 mod 16

### Lösung

Anwenden des Eulerschem Theorem

phi(16) = 8

Voraussetzung erfüllt, da 16 und 5 teilerfremd sind

5 hoch 8 = 1 (mod 16)

=> 5 hoch 292 = ((5 hoch 8) hoch 36) * 5 hoch 4 (mod 16)

 = 1 hoch 36 * 5 hoch 4 (mod 16)

 = 9 * 9 (mod 16)

 = 1 (mod 16)

## Aufgabe 2 (Klausur SS 2014)

Bestimme die ganzen Zahlen x und y möglichst effizient
 
482 x + 192 y = 4

### Lösung

482 = 2 * 192 + 98   (andere Schreibweise für 482 - darauf muss man erst einmal kommen)

192 = 1 * 98 + 94    (andere Schreibweise für 192)

94 = 23 * 4 + 2

4 = 2 * 2 => ggT = 2

# Jetzt Aufrollen von unten nach oben

2 = 94 - 23 * 4 mit 4 = 94 - 23 - 2 => 

  = 94 – 23 * (98 –94)

  = -23 * 98 + 24 * 94 mit 94 = 192 - 1 * 98 =>

  = -23 * 98 + 24 * (192 - 98)

  = 24 * 192 - 47 * 98 mit 98 = 482 - 2 * 192 =>

  = 24 * 192 - 47 * (482 - 2 * 192) 

  = 118 * 192 - 47 * 482

  = -47 * 482 + 118 * 192

Mit 4 = 2 * 2 =>

4 = -94 * 482 + 236 * 192

=> x = -94 und y = 236

## Aufgabe 3 (Klausur SS 2014)

Warum gibt es keine ganzen Zahlen x und y mit

482 x + 192y = 3 ?

### Lösung

Da sowohl 482 als auch 192 Vielfache von 2 sind, muss dass auch für eine ganzzahlige Linearkombination dieser beiden Zahlen gelten.

## Aufgabe 4 (ESA SS 24)

Berechnen Sie per Hand möglichst effizient

9 hoch 200 mod 23

Anwenden des Eulerschen Theorem:

9 φ(n) (1 mod n)

mit a = 9 und n = 23

Da n eine Primzahl ist, gilt φ(23) = 22

a und n sind teilerfremd, damit kann das Eulersches Theorem verwendet werden.

Der Exponent 200 wird mit der 22 zerlegt, in diesem Fall in 9 und Rest 2.

200 mod 22 = 200 - 9 * 22 = 200 - 198 = 2

=> 92 = 81

=> 81 mod 23 = 12

=> 9 hoch 200 = 12 mod 23

=> 9 hoch 200 mod 23 = 12

## Aufgabe 5 (ESA SS 24)

Berechnen Sie per Hand möglichst effizient

9 hoch 200 mod 38

Anwenden des Eulerschen Theorems mit a = 9 und n = 38

Beide sind teilerfremd, das Eulersche Theorem kann daher angewendet werden

φ(38) = 18

Vereinfachung durch Primfaktorenzerlegung von 38 in 2 und 19:

φ(2) = 1

φ(19) = 18

=> φ(38)  = 1 * 18

Reduktion des Exponenten

200 mod 18 = 200 − 11 x 18 = 200 − 198 = 2

=> 9 hoch 200 mod 38 = 92 mod 38

=> 9 hoch 2 = 81

=> 81 mod 38 = 81 − 2 x 38 = 81 − 76 = 5

=> 9 hoch 200 = 5 mod 38

=> 9 hoch 200 mod 38 = 5

## Aufgabe 6 (Präsenz 2)

Berechne per Hand möglichst effizient 7 hoch 157 mod 26

### Lösung

Anwenden des Eulerschem Theorem

geht, da ggT(157,26) = 1

phi(26) = (13-1) * (2-1) = 12

157 = 1 + 13 * 12

7 hoch 157 = 7 hoch 1 + 13 * 12 = 7 * (7 hoch 12) hoch 13 mod 26

Die "harte Tour": (7 mod 26) hoch 12 mod 26 und das 13 Mal hinschreiben und vorher noch 7 hoch 12 in 3 x 7 hoch 4 zerlegen...

Alternativ per Euler: a hoch phi(n) = 1 mod n

a = 7

n = 26

phi(26) = 12

7 hoch 12 = 1 mod 26

157 = 12 * 13 + 1

=> 157 mod 12 = 1

=> 7 hoch 157 mod 26

# Verstehe ich leider nicht? - das ist die erste Modulo-Aufgabe, die ich nicht lösen kann

=> 7 hoch 1 mod 26

=> 7 hoch 157  mod 26 = 7
 
 ChatGPT erklärt es mir dann noch einmal sehr ausführlich:

 ALles basiert auf Eulers Theorem

 7 hoch 12 = 1 mod 26

 impliziert, dass jede Hochzahl, die ein Vielfaches von 12 ist, sich auf 1 mod 26 reduzieren lässt.

 Daraus folgt

 7 hoch 157 = 7 hoch 12*13+1 = (7 hoch 12) hoch 13 * 7 hoch 1

 Und da 7 hoch 12 = 1 mod 26 

 => (1 mod 26) hoch 13 * 7 = 1 mod 26

 => 1 hoch 13 * 7 = 1 mod 26

 => 7 hoch 157 = 1 * 7 = 7 mod 26 = 7

Damit ist es dann doch klar. Das war aber eine der schwierigen Aufgaben, da 157 zum Zerlegen zu groß ist.

Alternativ kann es auch über die Binärdarstellung des Exponenten ausgerechnet werden:

157 = 1 0 0 1 1 1 0 1

=> 7 hoch (128 + 16 + 8 + 4  + 1)

=> 7 hoch 1 mod 26 = 7

=> 7 hoch 2 mod 26 = 7 * 7 mod 26 = 49 mod 26 = 23

=> 7 hoch 4 mod 26 = 23 * 23 mod 26 = 529 mod 26 = 9

=> 7 hoch 8 mod 26 = 9 * 9 mod 26 = 81 mod 26 = 3

=> 7 hoch 16 = 3 * 3 mod 26 = 9

=> 7 hoch 32 = 9 * 9 mod 26 = 3

=> 7 hoch 64 = 3 * 3 mod 26 = 9

=> 7 hoch 128 = 9 * 9 mod 26 = 3

7 hoch 157 = 7 hoch 128 



