# OMMI-WUK-SS24

Übungen für die Klausurvorbereitung für WUK

Themengebiete

1. Kombinatorik

2. Bedingte Wahrscheinlichkeiten (u.a. Bayes)

3. Verteilungen (Binomial, Hypergeometrisch)

4. Affine Chiffre

5. Modulo-Rechnungen mit großen Exponenten

6. RSA

7. Diffie-Hellmann

