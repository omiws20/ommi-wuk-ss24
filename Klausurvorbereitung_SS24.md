# Aufgaben für die Klausurvorbereitung

## Aufgabe 1 (Kombinatorik mit Student)

Ein Student muss in einer Prüfung 10 von 13 Fragen beantworten.

**a) Auf wie viele verschiedene Weisen kann er 10 Fragen auswählen (und beantworten)?**

Auf wie viele Weisen, falls er ...

**b) auf jeden Fall die beiden ersten Fragen beantworten muss?**

**c) er genau 3 der ersten 5 Fragen beantworten muss?**

**d) er mindestens 3 der ersten 5 Fragen beantworten muss?**

### Lösung

zu a)

Das ist Grundaufgabe 3 mit n = 13 und k = 10

Anzahl = (13 über 10) = 286

zu b)

Es gibt nur noch 11 Fragen und er wählt 8 Fragen aus.

Anzahl = (11 über 8) = 165

zu c)

Schritt 1: 3 aus 5
Anzahl1 = (5 über 3) = 10

Schritt 2: 8 über 7
Anzahl2 = (8 über 7) = 8
Anzahl = 10 * 8 = 80

zu d)

Jetzt wird es etwas "komplizierter", da drei Multimengen-Permutationen kombiniert werden müssen.

Anzahl = (5 über 3) * (8 über 7) + (5 über 4) * (8 über 6) + (5 über 5) * (8 über 5) = 276

## Aufgabe 2 (Wahrscheinlichkeiten mit zwei Würfeln)

Wie oft muss jemand mindestens mit zwei Würfeln werfen, damit die Wahrscheinlichkeit, dass ein
Doppelsechser unter den Würfen ist, größer ist als 50% ist?

### Lösung

**Schritt 1**: Berechnung der Wahrscheinlichkeit für eine Doppelsechs

Die Wahrscheinlichkeit, dass bei einem Wurf mit zwei Laplace-Würfeln beide Würfel eine 6 zeigen (Doppelsechser) ist:

P(Doppelsechser) = (1/6​) × (1/6​) = 1/36

**Schritt 2**: Wahrscheinlichkeit, keinen Doppelsechser zu werfen

P(KeinDoppelsechser) = 1 - 1/36 = 35/36

**Schritt 3**: Wahrscheinlichkeit, bei *n* Würfen keinen Doppelsechser zu werfen

P(KeinDoppelsechser nach n Versuchen) = (35/36) hoch n

**Schritt 4**: Wahrscheinlichkeit, bei *n* Würfen mindestens einen Doppelsechser zu werfen

P(EinDoppelsechser nach n Versuchen) = 1 - (35/36) hoch n

**Schritt 5**: Finden der kleinsten Anzahl *n*, für die diese Wahrscheinlichkeit größer als 50% ist

Gesucht ist das kleinste *n*, für das gilt:

1 - (35/36) hoch n > 0.5

Umformen der Ungleichung:

(35/36) hoch n < 0.5

Anwenden des natürlichen Logarithmus auf beiden Seiten:

ln((​(35/36) hoch n) < ln(0.5)

Mit ln(a hoch b) = b * ln(a):

n * ln(35/36) < ln(0.5)

Durch Teilen durch *ln(35/36)* und dem negativen Ergebnis kehrt sich die Ungleichung um:

n > ln(0.5) / ln(35/36)

Ausrechnen von ln(0.5) und ln(35/36):

n > −0.6931 / −0.0286

n > 24.22

Da *n* eine ganze Zahl sein muss, ist die kleinste Anzahl von Würfen, die erforderlich ist, damit die Wahrscheinlichkeit, mindestens einen Doppelsechser zu werfen, größer als 50% ist, n=25.

## Aufgabe 3 (Wahrscheinlichkeiten mit Kugeln einer Urne)

Eine Urne enthält 3 rote, 4 blaue und 5 grüne Kugeln. Jemand greift blind hinein und

**a) zieht 3 Kugeln gleichzeitig. Wie groß ist die Wahrscheinlichkeit, dass alle drei Kugeln die gleiche Farbe haben?**

**b) zieht 3 Kugeln gleichzeitig. Wie groß ist die Wahrscheinlichkeit, dass alle drei Kugeln unterschiedliche Farben haben?**

**c) zieht 3 Kugeln nacheinander mit Zurücklegen. Wie groß ist die Wahrscheinlichkeit, dass alle drei Kugeln die gleiche Farbe haben?**

**d) zieht 3 Kugeln nacheinander mit Zurücklegen. Wie groß ist die Wahrscheinlichkeit, dass alle drei Kugeln unterschiedliche Farben haben?**

### Lösung

zu a)

Bestimmen der Mächtigkeit der Ereignismenge der günstigen Ereignisse (also die Anzahl der günstigen Ereignisse).

Das ist Grundaufgabe 3 mit n = 12 und k = 3.

|A| = (12 über 3) = 220

Die Anzahl der möglichen günstigen Ereignisse für jede einzelne Farbe ergibt sich zu

|ARot| = (3 über 3) = 1

|ABlau| = (4 über 3) = 4

|AGrün| = (5 über 3) = 10

Da es sich um sich gegenseitig ausschließende Ereignisse handelt, werden die Wahrscheinlichkeiten addiert.

Damit ergibt sich

P(A) = 1/220 + 4/220 + 10/220 ≈ 0,06818=6,82%

Die Wahrscheinlichkeit, dass die drei gezogenen Kugeln dieselbe Farbe besitzen, beträgt 6.82%.

zu b)

Die Mächtigkeit der Gesamtmenge ergibt sich dieses Mal aus dem Produkt der Mächtigkeit der Einzelmengen für jede einzelne Farbe:

|A| = (3 über 1) * (4 über 1) * (5 über 1) = 60

Damit ergibt sich für die Wahrscheinlichkeit des Ereignisses

P(A) = 60/220 ≈ 0,2727=27,27 %

Die Wahrscheinlichkeit, dass drei gezogene Kugeln unterschiedliche Farben besitzen, beträgt 27,27%.

zu c) 

Da jede Kugel zurückgelegt wird, bleibt die Wahrscheinlichkeit für jede Ziehung gleich:

P(R) = 3/12 × 3/12 × 3/12 = 1/64

P(B) = 4/12 × 4/12 × 4/12 = 1/27

P(G) = 5/12 × 5/12 × 5/12 = 125/1728

Da die drei Ereignisse unvereinbar sind, werden die drei Einzelwahrscheinlichkeiten aufaddiert:

P(A) = 1/64 + 1/27 + 125/1728 = 0,125 = 12,5%

Die Wahrscheinlichkeit, dass die drei gezogenen Kugeln dieselbe Farbe besitzen, beträgt 12,5%.

zu d)

Da jede Kugel zurückgelegt wird, bleibt die Wahrscheinlichkeit für jede Ziehung gleich.

Da außerdem die Reihenfolge der drei Kugeln berücksichtigt werden muss, müssen die Wahrscheinlichkeiten mit 3! multipliziert werden.

Damit ergibt sich für die Wahrscheinlichkeit

P(A) = 3/12 × 5/12 × 4/12 × 6 ≈ 0,2083 =20,83%

Die Wahrscheinlichkeit, dass drei gezogene Kugeln unterschiedliche Farben besitzen, beträgt 20,83 %.

## Aufgabe 4 (Wahrscheinlichkeitsverteilung)

Eine Sportschützin trifft die Zielscheibe mit einer Wahrscheinlichkeit von 25%. Wenn sie viermal hintereinander schießt, wie groß ist die Wahrscheinlichkeit, dass sie

**a) die Zielscheibe mindestens einmal trifft?**

**b) die Zielscheibe genau zweimal trifft?**

**c) Wie oft muss sie schießen, damit die Wahrscheinlichkeit, dass sie mindestens einmal trifft, größer als 0,95 ist?**

### Lösung

zu a)

Aufstellen der Gegenwahrscheinlichkeit für keinen Treffer bei vier Versuchen:

P(KeinTreffer) = 1 - 0.25 = 0.75

Bei vier Versuchen beträgt die Wahrscheinlichkeit für keinen Treffer 

(3/4)^4 = 81/256 = 0.316

Die Wahrscheinlichkeit für mindestens einen Treffer ergibt sich aus der Gegenwahrscheinlichkeit:

P(Mind1Treffer) = 1 - 0.316 = 0.683

Die Wahrscheinlichkeit eines Treffers bei 4 Versuchen beträgt damit 68.3%.

zu b)

Bernoulli-Experiment mit gleicher Wahrscheinlichkeit für alle Ereignisse.

Anwenden der Binomialverteilung nach der allgemeinen Formel

P(X=k) = (n über k) × p^k × (1 - p) ^ (n-k)

mit 

n = 4
k = 2
p = 0.25

Die Wahrscheinlichkeit für zwei Treffer ergibt sich damit zu

P(X=2)= (4 über 2) × 0.25^2 × (1 - 0.25)^2 = 0.21 

Die Wahrscheinlichkeit für zwei Treffer bei 4 Versuchen beträgt damit 21.09%.

zu c)

Aufstellen der Wahrscheinlichkeitsgleichung für n Versuche und auflösen der Gleichung nach n:

**Schritt 1**: Vergleich mit gleich statt mit größer

P(min1) = 1- (3/4)^n = 0.95

**Schritt 2**: Auf beiden Seiten 1 subtrahieren

-(3/4)^n = -0.05

**Schritt 3**: Beide Seiten mit -1 multiplizieren

(3/4)^n= 0.05

**Schritt 4**: Anwendung des natürlichen Logarithmus auf beiden Seiten

ln(ln((3/4^n)) = ln⁡(0.05)

**Schritt 5**: Anwenden der Potenzregel des Logarithmus

n × ln(3/4) = ln⁡(0.05)

**Schritt 6**: Umstellen nach n

n = (ln⁡(0.05)) / (ln ln (0.75)) = 10.41

Die Schützin muss damit mindestens 11 Mal schießen, damit die Wahrscheinlichkeit für einen Treffer größer als 0.95 ist.

## Aufgabe 5 (Satz von Bayes - gezinkte Münzen)

Drei gezinkte Münzen M1, M2 und M3 haben die Wahrscheinlichkeiten 1/3, 2/3 und 1, dass Zahl erscheint. Jemand wählt zufällig eine der beiden Münzen aus und wirft sie in die Luft.

**a) Mit welcher Wahrscheinlichkeit ist es eine Zahl?**

**b) Falls es eine Zahl ist, mit welcher Wahrscheinlichkeit wurden M1, M2 bzw. M3 gewählt?**

### Lösung

zu a)

Die Gesamtwahrscheinlichkeit wird über den Satz der totalen Wahrscheinlichkeit berechnet.

P(M1) = 1/3

P(M2) = 2/3

P(M3) = 1

Die totale Wahrscheinlichkeit für das Werfen einer Zahl ist

P(Z) = P(M1) × P(M) + P(M2) × P(M) + P(M3) × P(M)

= 1/3  ×  1/3  +  1/3  × 2/3 + 1/3 × 3/3 = 6/9 = 66.67%

zu b) 

Ausgehend von der berechneten Gesamtwahrscheinlichkeit werden die Teilwahrscheinlichkeiten für M1, M2 und M3 über den Satz von Bayes berechnet.

Pz(Z|M1) = P(M1 ∩ Z) / P(Z) = 1/3 × 1/3 / 2/3 = 1/6

Pz(Z|M2) = P(M2 ∩ Z) / P(Z) = 1/3 × 2/3 / 2/3 = 1/3

Pz(Z|M3) = P(M3 ∩ Z) / P(Z) = 1/3 × 3/3 / 2/3 = 1/2


## Aufgabe 6 (Satz von Bayes - Erkrankungswahrscheinlichkeit nach Test)

Eine Person erhält einen positiven Test. Wie groß ist die Wahrscheinlichkeit dafür, tatsächlich erkrankt zu sein?

Es gibt zwei Elementarereignisse

Ereignis A: Die Person hat die Krankheit

Ereignis B: Das Testergebnis für die Krankheit ist positiv

Gesucht ist die Wahrscheinlichkeit dafür, dass die Person die Krankheit auch tatsächlich hat, wenn der Test positiv ist.

Die vorgegebenen Wahrscheinlichkeiten sind:

P(A) = 0.01    # Wahrscheinlichkeit für die Krankheit in der Bevölkerung

P(̅A) = 0.99    # Wahrscheinlichkeit dafür, die Krankheit nicht zu haben

P(B|A) = 0.99  # Wahrscheinlichkeit, dass der Test für einen Erkrankten positiv ist

P(B|̅A)= 0.05   # Wahrscheinlichkeit, dass der Test für einen Gesunden positiv ist (false positiv)

Gesucht ist P(A|B), d.h. die Wahrscheinlichkeit, dass die Person die Krankheit hat wenn der Test positiv ist.

### Lösung

**Schritt 1:** Berechnen der totalen Wahrscheinlichkeit (oder für einen positiven Test?)

P(B) = Wahrscheinlichkeit für eine Erkrankung

P(P) = P(B|A) * P(A) + P(B|̅A) * P(A)
     = 0.99 * 0.01 + 0.05 * 0.99
     = 0.0594

**Schritt 2:** Anwenden des Satz von Bayes

P(A|B) = P(B|A) * P(A) / P(B)
       = 0.99 * 0.01 / 0.0594 
       = 0.1667 ≅ 16.67%

Die Wahrscheinlichkeit für eine Erkrankung bei einem positiven Testergebnis beträgt damit "nur" 16.67%.

## Aufgabe 7 (Satz von Bayes - Pünktlichkeit)

Für die Mitarbeiter einer Firma stehen drei Verkehrsmittel zur Verfügung:

1. Auto
2. Fahrrad
3. ÖPVN

Die Wahrscheinlichkeit, dass ein Mitarbeiter M eines dieser Verkehrsmittel benutzt, beträgt jeweils:

P(A) = 0.5
P(F) = 0.2
P(Ö) = 0.3

Die Wahrscheinlichkeit, dass M bei der Verwendung eines drei Verkehrsmittel pünktlich zur Arbeit kommt, beträgt jeweils:

P(P|A) = 0.7
P(P|F) = 0.9
P(P|Ö) = 0.8

**a) Wie groß ist die Wahrscheinlichkeit, dass ein Mitarbeiter M pünktlich zur Arbeit kommt?**

**b) Wenn M unpünktlich kommt, wie groß ist die Wahrscheinlichkeit, dass er mit dem Fahrrad gekommen ist?**

### Lösung

zu a)

Berechnen der totalen Wahrscheinlichkeit für die Pünktlichkeit:

P(P) = P(P|A) * 1-P(A) + P(P|F) * 1-P(F) + P(P|Ö) * 1-P(Ö)
     = 0.7 * 0.5 + 0.9 * 0.2 + 0.8 * 0.3
     = 0.77 = 77%

Die Wahrscheinlichkeit, dass ein Mitarbeiter unabhängig von der Wahl des Verkehrsmittels pünktlich zur Arbeit kommt beträgt demnach 77%.

**Schritt 2:** Berechnen der Wahrscheinlichkeit für die Pünktlichkeit bei der Wahl des Fahrrades

Anwenden des Satz von Bayes:

P(F|P) = P(P|F) * P(F) / P(P)
       = 0.9 * 0.2 / 0.77
       = 0.2337

P(F|P) ist die Wahrscheinlichkeit für pünktliches Eintreffen.

Die Wahrscheinlichkeit, dass ein pünktlicher M das Fahrrad genommen hat, liegt damit bei 23.37%.

## Aufgabe 8 (Affine Chiffre)

Allgemein gilt

f(x) = a x + b mod 26

ABCD...Z -> 0,1,...25

Eine Sprachstatistik hat ergeben:

E -> Y =>  4 -> 24
N -> K => 13 -> 10

Damit ergeben sich zwei Modulo 26-Gleichungssysteme:

f(4)  = 24 mod 26
f(13) = 10 mod 26

Einsetzen in die Funktion

I   4a + b = 24 mod 26
II 13a + b = 10 mod 26

Auflösen des LSG, Multiplikation mit der Inversen (eine Division wäre nicht erlaubt!)

I-II 9a = -14 mod 26
III  9a =  12 mod 26

Berechnen der Inversen per erweitertem Euklid, da ggT(9, 26) = 1 (ansonsten müsste ausprobiert werden)

9 * 3 = 1 mod 26 -> 9 hoch -1 * 12 mod 26
-> a = 36 mod 26 = 10
a = 10

a einsetzen in LGS, z.B. in I
-> b = 24 mod 26 - 40 = -16

Addition von 26 -> b = 10

Gibt es für a noch eine weitere Lösung?

Konkret, welche Zahl ergibt mit 13 multipliziert, einem + 10 und einer Division durch 26 einen Rest 10?

II 130 + 10 = 10 mod 26

Durch Ausprobieren ergibt sich a=23

Die Lösungen für a sind damit 10 und 23 und für b 10.

## Aufgabe 9 (Exponenten-Modulo 1)

Berechne per Hand möglichst effizient 5 hoch 292 mod 16

### Lösung

Anwenden des Eulerschem Theorem

phi(16) = 8

Voraussetzung erfüllt, da 16 und 5 teilerfremd sind, d.h. ggT(16,5) = 1

5 hoch 8 = 1 (mod 16)

=> 5 hoch 292

 = ((5 hoch 8) hoch 36) * 5 hoch 4 (mod 16)

 = 1 hoch 36 * 5 hoch 4 (mod 16)

 = 625 hoch 4 (mod 16)

 = ((25 hoch 2 mod 16) * (25 hoch 2 mod 16)) mod 16

 = (1 * 1) mod 16

( = 9 * 9 (mod 16)) // Aus Musterlösung? Wieso 9 * 9?

 = 1 (mod 16)

## Aufgabe 10 (Exponenten-Modulo 2)

Berechne die Inverse von 155 modulo 257

### Lösung

Die Inverse ist 155 hoch -1 (mod 257)

Wegen ggT(155, 257) = 1 ist die Aufgabe lösbar

Gesucht sind damit ganze Zahlen x und y für die gilt:

155 * x + 257 * y = 1

x ist dann (modulo 257) die gesuchte Inverse von 155

Anwenden des euklidischen Algorithmus für die Zahlen 257 und 155:

257   = 1 * 155 + 102

155   = 1 * 102 +  53

102   = 1 *  53 +  49

53    = 1 *  49 +   4

49    = 12 * 4 + 1  (1 muss da stehen wg. ggT)

Rückwärtsauflösen der Gleichungen:

1 = 49 - 12 * 4

  = 49 - 12 * (53-49) = -12 * 53 + 13 * 49

  = -12 * 53 + 13 * (102-53) = 13 * 102 - 25 * 53

  = 13 * 102 - 25 * (155 - 102) = -25 * 155 + 38 * 102
 
  = -25 * 155 + 38 * (257 - 155) = 38 * 257 - 63 * 155

  = 38 * 257 - 63 * 155

Demnach gilt x = 155 hoch -1 (mod 257) = -63 (mod 257)

Durch Addition von 257 wird aus der -63 eine 194.

x = 194 (mod 257) => x = 194.

## Aufgabe 11 (RSA - Lauschangriff)

In einem Public Key-System nach dem RSA-Verfahren fangen Sie als blöder Lauscher die verschlüsselte Nachricht C=10 ab, die an den Empfänger gerichtet ist, dessen Public Key aus e=5 und m=35 (Modul) besteht.

Wie lautet die Klartextnachricht *Z*?

### Lösung

Das Modul *m* kann in die Primzahlen 5 und 7 verlegt werden, daher beträgt der "Hilfsmodul" (in anderen Lösungen *phi*)

m* = 4*6 = 24

Für den privaten Schlüssel gilt

d = e hoch -1 mod 24

= 5 hoch -1 mod 24

= 5

Mit *d* lässt sich die Nachricht *C* problemlos in Nullkommanix entschlüsseln:

z = c hoch d (mod 35)

= 10 hoch 5 (mod 35)

= 5

Wenn es so klar ist, dauert das Hinschreiben keine 5 Minuten;)

## Aufgabe 12 (RSA - Chiffre entschlüsseln)

Zur Übermittlung geheimer Nachrichten verwenden Bob und Alice das RSA-Verfahren.

Bob verfügt natürlich über die Primfaktorzerlegung von m = 55 und kann somit φ(m) berechnen.

Als öffentlichen Schlüssel verwendet Bob e = 23.

**a) Ist Bobs Schlüssel überhaupt ein zulässiger Wert?**

**b) Bob sendet diesen Schlüssel sofort an Alice, da sie ihm etwas sehr Wichtiges mitteilen will.

Alice verschlüsselt ihre Klartextnachricht K (natürliche Zahl < 55) mit diesem Schlüssel. Es ergibt sich der Geheimtext C = 9. Wie lautet der Klartext?**

### Lösung

zu a)

Der Wert e = 23 muss die Anforderungen des RSA-Algorithmus erfüllen:

1. 1 < e < φ(m)

Als erstes wird die Eulersche Phi-Funktion verwendet.

m = 55 kann in die Primzahlen 5 und 11 zerlegt werden.

Damit wird φ(55) berechnet:

φ(m) = (p − 1) × (q − 1) = (5 − 1) × ( 11 − 1) = 4 × 10 = 40

Dies erste Bedingung ist erfüllt, denn 1 < 23 < 40 

2. Der größte gemeinsame Teiler (ggT) von e und φ(m) muss 1 sein

Durch Anwenden des Euklidischen Algorithmus ergibt sich ggT(23, 40) = 1

Der größte gemeinsame Teiler von e und φ(m) ist 1, deshalb ist die zweite Bedingung ebenfalls erfüllt.

Da beide Bedingungen erfüllt sind, ist e = 23 ein durchaus zulässiger Wert für Bob’s Schlüssel.

zu b)

Bekannt sind:

- m = 55 für die Primfaktorzerlegung und Berechnen von phi(n)

- e für den öffentlichen Schlüssel = 23

- Geheimtext C = 9

Gesucht wird:

- Die Klartextnachricht K

Der Public Key ist (m, e) = (55, 23)

**Schritt 1**: Primfaktorzerlegung von 55 in die Primzahlen 5 und 11

Berechnen der positiven ganzen Zahlen kleiner oder gleich *m*, die teilerfremd zu *m* sind, mit Hilfe der Eulerschen phi-Funktion:

φ(n) = (5-1) * (11-1) = 40

Für die Entschlüsselung wird die Variable *d* aus dem privaten Schlüssel berechnet.

Für *d* gilt die Bedingung *e * d ≡ 1 (mod φ(m))*

*d* muss als multiplikatives Invers von *e mod φ(m)* berechnet werden.

Dies geschieht entweder durch Ausprobieren oder über den erweiterten euklidischen Algorithmus mit *a=23* und *b=40*.

40 = 1 * 23 + 17

23 = 1 * 17 + 6

17 = 2 * 6 + 5

6 = 1 * 5 + 1

5 = 5 * 1 + 0

Rückwärtseinsetzen für die Bestimmung des Inversen:

1 = 6 - 1 * 5

= 6 - 1 (17 - 2 * 6)

= 3 * 6 - 1 * 17

= 3 (23- 17) - 1 * 17

= 3 * 23 - 4 * 17

= 3 * 23 - 4 * (40 - 23)

= 7 * 23 - 4 * 40

Daraus folgt

7 * 23  ≡ 1 (mod 40)

und aus

7 * 23 - 4 * 40

d = 7

Mit *d* kann der Geheimtext über den privaten Schlüssel entschlüsselt werden:

K = 9 hoch 7 mod 55

9 hoch 7 mod 55 wird wie folgt berechnet:

9 hoch 2 = 81 ≡ 26 (mod 55)

9 hoch 4 = 262 = 676  ≡ 16 (mod 55)

9 hoch 7 = 9 * 96 = 9 * (94 * 92) = 9 * (16 * 26) (mod 55)

16 * 26 = 416  ≡ 31 (mod 55)

9 hoch 7 = 9 * 31 = 279  ≡ 4 (mod 55) = 4

Der Klartext ist damit 4.

## Aufgabe 13 (Diffie-Hellmann)

Alice und Bon möchten einen geheimen Schlüssel über einen extrem unsicheren Kanal austauschen, indem sie das Diffie-Hellman-Schlüsselaustauschverfahren verwenden. 

Sie einigen sich auf die gemeinsamen öffentlichen Werte p und g, wobei p eine Primzahl und g eine ganzzahlige Basis ist.

Sie wählen p = 23 (Primzahl) und q = 5 (ganzzahlige Basis).

Danach wählen beide eine Geheimzahl a und b:

a =  6 (Alice)

b = 15 (Bon)

**a) Berechne den öffentlichen Schlüssel, den Alice an Bon sendet**

**b) Berechne den öffentlichen Schlüssel, den Bon an Alice sendet**

**c) Berechne den gemeinsamen geheimen Schlüssel, den sowohl Alice als auch Bon am Ende des Austauschs haben**

## Lösung

zu a)

A = q hoch a mod p = 5 hoch 6 mod 23 = 8

zu b)

B = q hoch b mod p = 5 hoch 15 mod 23 = 19

zu c)

Beide Parteien berechnen den gemeinsamen geheimen Schlüssel.

Alice berechnet den geheimen Schlüssel S mit Bons öffentlichem Schlüssel und ihrer geheimen Zufallszahl a:

Sa = B hoch a mod p =  19 hoch 6 mod 23 = 2

Bon berechnet wiederwillig den geheimen Schlüssel S mit Alices öffentlichem Schlüssel und seiner geheimen Zufallszahl b:

Sb = A hoch b mod p = 8 hoch 15 mod 23 = 2

Der geheime Schlüssel von Bon und Alice ist damit 2.

Bon nennt sich danach in Bob um.
