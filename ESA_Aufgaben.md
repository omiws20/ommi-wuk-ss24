# ESA-Aufgaben

Hier habe ich Einsendeaufgaben aus zwei Semestern zusammengestellt.

# SS23
*** noch nicht vollständig

## ESA1

### Aufgabe 1

Gegeben ist das Wort BAFANABAFANA. 

**a) Wie viele Wörter lassen sich durch Buchstabenvertauschungen bilden?**
**b) Wie viele dieser Wörter beginnen mit BA und enden mit NA?**
**c) Wie viele dieser Wörter lassen sich so erzeugen, dass die drei Buchstaben B, F und N nur paarweise nebeneinander auftreten?**

### Lösung

Das Wort besteht aus den vier Buchstaben A,B,F und N und ist zwölf Zeichen lang**

Bei der Berechnung der Kombinationsmöglichkeiten kommt es auf die Häufigkeit der einzelnen Buchstaben an.

A	6
B	2
F	2
N	2

Das A kommt 6 Mal, die drei anderen Buchstaben kommen jeweils 2x vor.

zu a)

Die Anzahl der Wörter, die sich durch Buchstabenvertauschungen bilden lassen, ergibt sich zu

Anzahl = 12! / (6! ∙2! ∙2! ∙2!) = 83.160

Im Zähler steht die Wortlänge, im Nenner das Produkt der Fakultäten der Häufigkeit jedes der vorkommenden Buchstaben.

zu b)

In dieser Einschränkung werden nur noch acht der zwölf Buchstabenplätze frei belegt, da die Plätze 1 und 2 sowie 11 + 12 bereits mit den Buchstabenpaaren BA und NA belegt sind. Außerdem kommt der Buchstabe A für das verbleibende Wort nur noch vier Mal, und die Buchstaben B und N jeweils nur einmal vor. Es bleiben die Buchstaben AAAAFFBN übrig.

Die Anzahl der Wörter, die sich bilden lassen, ergibt sich zu

Anzahl = 8!/(4! ∙2! ∙1! ∙1!)= 840

zu c)
Durch die Paarbildung reduziert sich die Anzahl der Buchstaben auf sechs Mal A und jeweils einmal BB, FF und NN für 9 statt 12 Plätze.

Die Anzahl der Wörter, die sich bilden lassen, ergibt sich zu

Anzahl = 9!/(6! ∙1! ∙1! ∙1!)= 504

## Aufgabe 2

Ein Student soll aus zehn Aufgaben sieben Aufgaben aussuchen.

a) Anzahl der Wahlmöglichkeiten ohne Einschränkungen
b) Der Student soll in jedem Fall die ersten beiden Aufgaben auswählen
c) Der Student soll die erste oder zweite Aufgabe auswählen, aber nicht beide
d) Der Student muss von den ersten fünf Aufgaben genau drei Aufgaben auswählen

## Lösung

*** Noch nicht mit der Korrektur abgeglichen

zu a)

Das ist Grundaufgabe 3 mit n = 10 und k = 7.

Die Anzahl der Wahlmöglichkeiten beträgt

Anzahl = (10 über 7) = 120

zu b)

Von den zehn Aufgaben bleiben acht Aufgaben zur freien Wahl übrig, aus denen fünf gewählt werden, d.h. Grundaufgabe 3 mit n = 8 und k = 5.

Die Anzahl der Wahlmöglichkeiten beträgt

Anzahl = (8 über 5) = 56

zu c)

Es gibt zwei Auswahlgruppen. Der Student hat für die erste Gruppe zwei und für die verbleibende Gruppe sechs Möglichkeiten.
Es liegt damit eine Kopplung zweier Ziehungen vor:

Anzahl = (2 über 1) * (8 über 6) = 56

zu d)

Von den zehn Aufgaben verbleiben fünf Aufgaben, aus denen "er" vier Aufgaben auswählen muss.
Er muss aus den ersten fünf drei und aus den verbleibenden fünf vier Aufgaben auswählen.

Es ergibt sich eine Kopplung der beiden Ziehungen:

Anzahl = (5 über 3) * (5 über 4) = 50

## Aufgabe 3

Die Parteien A, B und C senden n Vertreter an einen runden Tisch mit 3n Plätzen.

Bei n = 4 sind es z.B. 12 Plätze.

Ohne Einschränkungen gibt es 3n! verschiedene Sitzordnungen, z.B. 72 bei n = 4

a) Alle A-ler, B-ler und C-ler sollen nebeneinander sitzen
b) Die Sitzreihe soll im Uhrzeigersinn beginnend bei einem beliebigen A periodisch fortgesetzt werden, z.B. bei n = 3
c) Alle A-ler sollen jeden dritten Platz belegen

zu a)

Es gibt drei Gruppen, innerhalb jeder Gruppe gibt es n! Sitzmöglichkeiten (2. Grundaufgabe mit n=k).

Da es drei Gruppen gibt und eine Kopplung der Permutationen pro Gruppe vorliegt, ergeben sich insgesamt 

n! * n! * n! = n! 3 Sitzmöglichkeiten.

Bei n = 4 sind es 13.824 Sitzmöglichkeiten

zu b)

Beispiel: A B C A B C A B C A B C

Eine Voraussetzung ist, dass die Folge erhalten bleibt, die Platzierungen nach dem A aber variieren.
Die entstehenden 3er Gruppen können nur noch eingeschränkt variiert werden.

Pro Gruppe mit jeweils drei Vertretern ergeben sich drei Möglichkeiten:

A B C
C A B
B A C

Damit ergibt sich die allgemeine Anzahl der Möglichkeiten zu

n! * 3

Da wieder eine Kopplung vorliegt, wird multipliziert.

zu c)

Die Anzahl der Gruppen ist wieder n!
Am Beispiel einer 3er Gruppe gibt es pro Gruppe drei (allgemein n) Möglichkeiten:
A B C
B C A
C B A
Allgemein beträgt die Anzahl an Möglichkeiten:
n! * (2n)!
Es gibt für die Aler n! Möglichkeiten der Platzierung.
Für die anderen beiden Parteien gibt es dagegen (2n)! Möglichkeiten.
Es sind nur noch 2n Personen vorhanden und es spielt keine Rolle, wo sie sich hinsetzen.
Für n = 4 ergibt sich für die Anzahl an Möglichkeiten:


## ESA2

## Aufgabe 1
Wie oft müssen Sie mindestens mit zwei Würfeln werfen, damit die Wahrscheinlichkeit, dass ein
Doppelsechser unter den Würfen ist, größer ist als 50 %

### Lösung

Gesucht ist die Anzahl der Würfe mit zwei (Laplace-) Würfeln, die erforderlich ist, damit die
Wahrscheinlichkeit für zwei Sechser > 50% ist

Aus dem Ereignisbaum für zwei Würfe ergeben sich folgende Wahrscheinlichkeiten:

xxx

## Aufgabe 2

Anne und Lena spielen Tennis. Anne gewinnt einen Satz mit der Wahrscheinlichkeit 𝑝 = 2/3. Die beiden
spielen so viele Sätze hintereinander, bis eine von ihnen erstmals zwei Sätze direkt hintereinander gewinnt.

a) Wie groß ist die Wahrscheinlichkeit, dass Anne ein 3-Satz-Spiel gewinnt?
b) Wie viele Sätze kommen (bei unbegrenzter Anzahl von Sätzen) im Mittel zustande?

### Lösung
zu a)

zu b)

## Aufgabe 3

Eine Urne enthält 3 rote, 4 blaue und 5 grüne Kugeln. Sie greifen blind hinein und…

a) ziehen 3 Kugeln gleichzeitig. Wie groß ist die Wahrscheinlichkeit, dass alle drei
Kugeln die gleiche Farbe haben?
b) ziehen 3 Kugeln gleichzeitig. Wie groß ist die Wahrscheinlichkeit, dass alle drei
Kugeln unterschiedliche Farben haben
c) ziehen 3 Kugeln nacheinander und mit zurücklegen. Wie groß ist die
Wahrscheinlichkeit, dass alle drei Kugeln die gleiche Farbe haben?
d) ziehen 3 Kugeln nacheinander und mit zurücklegen. Wie groß ist die
Wahrscheinlichkeit, dass alle drei Kugeln unterschiedliche Farben haben?

### Lösung
zu a)

zu b)

zu c)

## Aufgabe 4

Drei gezinkte Münzen 𝑀1, 𝑀2 und 𝑀3 haben die Wahrscheinlichkeiten 1/3, 1/2 und 1, dass Zahl
erscheint. Sie wählen zufällig eine der Münzen aus und werfen sie

a) Mit welcher Wahrscheinlichkeit werfen Sie Zahl?
b) Falls Sie Zahl werfen, mit welcher Wahrscheinlichkeit haben Sie 𝑀1, 𝑀2 bzw. 𝑀3
gewählt?

### Lösung
zu a)

zu b)


## ESA3

## Aufgabe 1

Man zeige:

xxx fehlt noch

### Lösung

## Aufgabe 2


Bobs öffentlicher RSA-Schlüssel lautet (𝑚 = 299, 𝑒 = 53).

Alice verschlüsselt ihre Klartextnachricht 𝑧 (𝑧 ∈ ℕ, 𝑧 < 299) mit diesem Schlüssel, und es ergibt sich
der Geheimtext 𝑐 = 200. Wie lautet der Klartext?

### Lösung

## Aufgabe 3 (Affine Chiffre)

xxx


# SS24

## ESA1

### Aufgabe 1 (SS24)

Gegeben sind die Buchstaben X, X, X, X, Y, Y, Z, Z, Z. 

**a) Wie viele verschiedene Anordnungen dieser Buchstaben gibt es?**

Es handelt sich um eine Permutation mit Multimengen. Es gibt 9 Buchstaben, aus denen sich n Anordnungen bilden lassen.

Bei der Berechnung der Zahl n als die Anzahl der Kombinationsmöglichkeiten kommt es auf die Häufigkeit der einzelnen Buchstaben an:

X	4
Y	2
Z	3

Die Anzahl der Wörter, die sich durch Buchstabenvertauschungen bilden lassen, ergibt sich zu

9! / 4! * 3! * 2! = 1260

Im Zähler steht die Wortlänge, im Nenner das Produkt der Fakultäten der Häufigkeit jedes der vorkommenden Buchstaben.

**b) Wie viele dieser Buchstabenfolgen beginnen mit einem X und enden mit einem Z?**

In dieser Einschränkung werden nur noch 7 der 9 Buchstabenplätze frei belegt, da der erste und der letzte Platz bereits mit den Buchstaben X und Z belegt sind. Beide Buchstaben kommen daher für das verbleibende Wort ein mal weniger vor.

Es bleiben die Buchstaben X, X, X, Y, Y, Z, Z übrig:

Die Anzahl der Wörter, die sich bilden lassen, ergibt sich zu

7! / 3! * 2! * 2! = 210

**c) Wie viele dieser Buchstabenfolgen gibt es, bei denen die vier X direkt nebeneinanderstehen?**

Durch die Einführung der 4er-Gruppe reduziert sich die Anzahl der Buchstaben auf fünf plus die Gruppe, die als eigener Buchstabe behandelt wird. Es bleiben die Buchstaben Y,Y,Z,Z,Z übrig.

Die Anzahl der sich bilden lassenden Wörter ergibt sich zu

6! / 2! * 3! * 1 = 60

### Aufgabe 2 (SS24)

Eine Eisdiele in Esslingen bietet 10 Eissorten an. Wie viele Zusammenstellungen von Eissorten für eine Waffel mit 3 Kugeln gibt es (ohne Berücksichtigung der Reihenfolge), wenn 

**a) alle 3 Kugeln von unterschiedlicher Sorte sein sollen?**

Es liegt eine Kombination (die Reihenfolge spielt keine Rolle) ohne Zurücklegen vor.

Das ist Grundaufgabe 3 mit n = 10 und k = 3

Anzahl = (10 über 3) = 120

**b) die Waffel genau 2 verschiedene Sorten enthalten soll?**

Die Anzahl muss in zwei Schritten berechnet werden.

Im ersten Schritt wird die Anzahl an Kombinationen für 2 verschiedene Eissorten berechnet.

Das ist Grundaufgabe 3 mit n = 10 und k = 20

Anzahl = (10 über 2) = 45

Im zweiten Schritt geht es um die dritte Kugel. Für die dritte Kugel gibt es nur noch zwei Möglichkeiten, da es nur zwei unterschiedliche Sorten geben darf.

Das ist Grundaufgabe 3 mit n = 2 und k = 1

Anzahl = (2 über 1) = 2

Gesamt = 45 * 2 = 90

**c) beliebige Zusammenstellungen von Sorten zulässig sind?**

Das ist Grundaufgabe 4 mit n = 10 und k = 3

Anzahl = (10 + 3 - 1 über 3) = (12 über 3) = 220

### Aufgabe 3 (SS24)

Jemand kauft 5 Grußkarten in einem Papierwarengeschäft, das 4 verschiedenen Kartenmotive anbietet, die dem Käufer gefallen.

**a) In wie vielen Motiven kann der potentielle Käufer die 5 Karten zusammenstellen?**

Annahme: Mit “Motiv” in der Fragestellung ist “Kombination” gemeint

Das ist Grundaufgabe 4 mit n = 4 und k = 2

Anzahl = (4 + 5 - 1 über 5) = 56

**b) Wie viele der möglichen Kombinationen enthalten genau 2 der 4 Motive?**

Berechnung in zwei Schritten.

Im ersten Schritt werden die Anzahl der Kombinationen für zwei Motive berechnet.

Das ist Grundaufgabe 3 mit n = 4 und k = 2

Anzahl = (4 über 2) = 6

Im zweiten Schritt werden die restlichen 3 Karten gewählt. Der Werteraum ist 2, da diese Karten die gleichen Motive haben müssen wie die ersten beiden.

Das ist Grundaufgabe 4 mit n = 2 und k = 3

Anzahl = (2 + 3 - 1 über 3) = 4

Damit gibt es 4 Kombinationen aus 3 Karten, die eine Kombination aus 2 unterschiedlichen Motiven zu einer Auswahl von 5 Karten vervollständigen.

Gesamt = 6 * 4 = 24

### Aufgabe 4 (SS24)

Ein Kartenspiel besteht aus den vier Kartenwerten Bube, Dame, König, Ass in allen vier Farben Kreuz, Pik, Herz, Karo – es enthält damit 16 verschiedene Karten.

**a) Wie viele verschiedene Möglichkeiten gibt es, daraus (für drei Spieler A, B, C) drei Blätter zu je drei Karten zu verteilen?**

Das ist Grundaufgabe 3, die für jeden der drei Spieler mit entsprechenden Werte angewendet wird.

Für Spieler 1 mit n = 16 und k = 3

Anzahl1 = (16 über 3) = 560

Für Spieler 2 mit n = 13 und k = 3

Anzahl2 = (13 über 3) = 286

Für Spieler 3 mit n = 10 und k = 3

Anzahl3 = (10 über 3) = 120

Anzahl = 560 * 286 * 120 = 19.219.200

**b) Wie viele solche Möglichkeiten gibt es, wenn jeder Spieler nur Karten derselben Farbe erhält?**

|Farbe 1|Farbe 2|Farbe 3|Farbe 4|Verfügbare Farben|
|-------|-------|-------|-------|-----------------|
|1. Blatt|4 über 3|4 über 3|4 über 3|4 über 3|4|
|2. Blatt|4 über 3|4 über 3|4 über 3||3|
|3. Blatt|4 über 3|4 über 3|||2|

Blatt 1: Es können 3 von 4 Kartenmotiven aus allen 4 Farben ausgewählt werden

Anzahl1 = 4 * (4 über 3) = 16

Blatt 2: Es können 3 von 4 Kartenmotiven aus 3 Farben ausgewählt werden

Anzahl2 = 3 * (4 über 3) = 12

Blatt 3: Es können 3 von 4 Kartenmotiven aus 2 Farben ausgewählt werden

Anzahl3 = 2 * (4 über 3) = 8

Gesamt = Anzahl1 * Anzahl2 * Anzahl3 = 1536

Für den zweiten Spieler stehen noch 13 Karten zur Auswahl:

c) Wie viele solche Möglichkeiten gibt es, falls jedes der drei Blätter genau ein Ass enthalten soll?

|         |Kombinationen Asse|Kombinationen restliche Karten|Kombinationen insgesamt|
|---------|------------------|------------------------------|-----------------------|
|1. Blatt |	(4 über 1) = 4	 |  (12 über 2) = 66	        |    66 * 4 = 264       |
|2. Blatt |	(3 über 1) = 3	 |  (10 über 2) = 45	        |    45 * 3 = 135       |
|3. Blatt |	(2 über 1) = 2	 |  (8 über 2) = 28             |    28 * 2 = 56        |

Gesamt = 264 * 135 * 56 = 1.995.840

d) Wie viele Verteilungsmöglichkeiten gibt es, so dass jeder der drei Spieler genau ein Ass, einen König und eine Dame in Händen hält? 

|         |Kombinationen Asse|Kombinationen restliche Karten|Kombinationen insgesamt|
|---------|------------------|------------------------------|-----------------------|
|1. Blatt |	(4 über 1) = 4	 |  (4 über 2) = 4	            |    4³ = 64            |
|2. Blatt |	(3 über 1) = 3	 |  (3 über 2) = 3	            |    3³ = 27            |
|3. Blatt |	(2 über 1) = 2	 |  (2 über 2) = 2              |    2³ = 8             |

Gesamt = 64 * 27 * 8 = 13.824

## ESA2

### Aufgabe 1 (SS24)

Eine Urne enthält 3 rote, 4 blaue und 5 grüne Kugeln. Sie greifen blind hinein und

**a) ziehen 3 Kugeln gleichzeitig. Wie groß ist die Wahrscheinlichkeit, dass alle drei Kugeln die gleiche Farbe haben?**

Bestimmen der Mächtigkeit der Ereignismenge der günstigen Ereignisse (also die Anzahl der günstigen Ereignisse).

Das ist Grundaufgabe 3 mit n = 12 und k = 3.

|A| = (12 über 3) = 220

Die Anzahl der möglichen günstigen Ereignisse für jede einzelne Farbe ergibt sich zu

|ARot| = (3 über 3) = 1
|ABlau| = (4 über 3) = 4
|AGrün| = (5 über 3) = 10

Da es sich um sich gegenseitig ausschließende Ereignisse handelt, werden die Wahrscheinlichkeiten addiert.

Damit ergibt sich 

P(A) = 1/220 + 4/220 + 10/220 ≈ 0,06818=6,82%

Die Wahrscheinlichkeit, dass die drei gezogenen Kugeln dieselbe Farbe besitzen, beträgt 6.82%.

**b) ziehen 3 Kugeln gleichzeitig. Wie groß ist die Wahrscheinlichkeit, dass alle drei Kugeln unterschiedliche Farben haben?**

Die Mächtigkeit der Gesamtmenge ergibt sich dieses Mal aus dem Produkt der Mächtigkeit der Einzelmengen für jede einzelne Farbe:

|A| = (3 über 1) * (4 über 1) * (5 über 1) = 60

Damit ergibt sich für die Wahrscheinlichkeit des Ereignisses

P(A) = 60/220 ≈ 0,2727=27,27 %

Die Wahrscheinlichkeit, dass drei gezogene Kugeln unterschiedliche Farben besitzen, beträgt 27,27%.

**c) ziehen 3 Kugeln nacheinander mit Zurücklegen. Wie groß ist die Wahrscheinlichkeit, dass alle drei Kugeln die gleiche Farbe haben?**

Da jede Kugel zurückgelegt wird, bleibt die Wahrscheinlichkeit für jede Ziehung gleich:

P(R) = 3/12 × 3/12 × 3/12 = 1/64
P(B) = 4/12 × 4/12 × 4/12 = 1/27
P(G) = 5/12 × 5/12 × 5/12 = 125/1728

Da die drei Ereignisse unvereinbar sind, werden die drei Einzelwahrscheinlichkeiten aufaddiert:

P(A) = 1/64 + 1/27 + 125/1728 = 0,125 = 12,5%

Die Wahrscheinlichkeit, dass die drei gezogenen Kugeln dieselbe Farbe besitzen, beträgt 12,5%.

**d) ziehen 3 Kugeln nacheinander mit Zurücklegen. Wie groß ist die Wahrscheinlichkeit, dass alle drei Kugeln unterschiedliche Farben haben?**

Da jede Kugel zurückgelegt wird, bleibt die Wahrscheinlichkeit für jede Ziehung gleich.

Da außerdem die Reihenfolge der drei Kugeln berücksichtigt werden muss, müssen die Wahrscheinlichkeiten mit 3! multipliziert werden.

Damit ergibt sich für die Wahrscheinlichkeit

P(A) = 3/12 × 5/12 × 4/12 × 6 ≈ 0,2083 =20,83%

Die Wahrscheinlichkeit, dass drei gezogene Kugeln unterschiedliche Farben besitzen, beträgt 20,83 %.

### Aufgabe 2 (SS24)

Drei gezinkte Münzen M1, M2 und M3 haben die Wahrscheinlichkeiten 1/3, 2/3 und 1, dass Zahl erscheint. Sie wählen
zufällig eine der Münzen aus und werfen sie.

**a) Mit welcher Wahrscheinlichkeit werfen Sie Zahl?**

Die Gesamtwahrscheinlichkeit wird über den Satz der totalen Wahrscheinlichkeit berechnet.

P(M1) = 1/3
P(M2) = 2/3
P(M3) = 1

Die totale Wahrscheinlichkeit ist

P(Z) = P(M1) × P(M1) + P(M2) × P(M2) + P(M3) × P(M3)
     = 1/3  ×  1/3  +  1/3  ×2/3+  1/3  ×3/3=  6/9  = 66.67%

**b) Falls Sie Zahl werfen, mit welcher Wahrscheinlichkeit haben Sie M1, M2 bzw. M3 gewählt?**

Ausgehend von der berechneten Gesamtwahrscheinlichkeit werden die Teilwahrscheinlichkeiten für M1, M2 und M3 über den Satz von Bayes berechnet.

Pz(M1) = P(M1 ∩ Z) / P(Z) = 1/3 × 1/3 / 2/3 = 1/6

Pz(M2) = P(M2 ∩ Z) / P(Z) = 1/3 × 2/3 / 2/3 = 1/3

Pz(M3) = P(M3 ∩ Z) / P(Z) = 1/3 × 3/3 / 2/3 = 1/2


### Aufgabe 3 (SS24)

Eine Sportschützin trifft die Zielscheibe mit einer Wahrscheinlichkeit von 25%. Wenn sie viermal hintereinander schießt, wie groß ist die Wahrscheinlichkeit, dass sie

**a) die Zielscheibe mindestens einmal trifft?**

Aufstellen der Gegenwahrscheinlichkeit für keinen Treffer bei vier Versuchen:

P(KeinTreffer) = 1 - 0.25 = 0.75

Bei vier Versuchen beträgt die Wahrscheinlichkeit für keinen Treffer 

(3/4)^4 = 81/256 = 0.316

Die Wahrscheinlichkeit für mindestens einen Treffer ergibt sich aus der Gegenwahrscheinlichkeit:

P(Mind1Treffer) = 1 - 0.316 = 0.683

Die Wahrscheinlichkeit eines Treffers bei 4 Versuchen beträgt damit 68.3%.

**b) die Zielscheibe genau zweimal trifft?**

Anwenden der Binomialverteilung nach der allgemeinen Formel

P(X=k)= (n über k) × p^k × (1 - p) ^ (n-k)

mit 

n = 4
k = 2
p = 0.25

Die Wahrscheinlichkeit für zwei Treffer ergibt sich damit zu

P(X=2)= (4 über 2) × 0.25^2 × (1 - 0.25)^2 = 0.21 

Die Wahrscheinlichkeit für zwei Treffer bei 4 Versuchen beträgt damit 21.09%.

**c) Wie oft muss sie schießen, damit die Wahrscheinlichkeit, dass sie mindestens einmal trifft, größer als 0, 95 ist?**

Aufstellen der Wahrscheinlichkeitsgleichung für n Versuche und auflösen der Gleichung nach n:

**Schritt 1**: Vergleich mit gleich statt mit größer

P(min1) = 1- (3/4)^n = 0.95

**Schritt 2**: Auf beiden Seiten 1 subtrahieren

-(3/4)^n = -0.05

**Schritt 3**: Beide Seiten mit -1 multiplizieren

(3/4)^n= 0.05

**Schritt 4**: Anwendung des natürlichen Logarithmus auf beiden Seiten

ln(ln((3/4^n)) = ln⁡(0.05)

**Schritt 5**: Anwenden der Potenzregel des Logarithmus

n × ln(3/4) = ln⁡(0.05)

**Schritt 6**: Umstellen nach n

n = (ln⁡(0.05)) / (ln ln (0.75))  = 10.41

Die Schützin muss damit mindestens 11 Mal schießen, damit die Wahrscheinlichkeit für einen Treffer größer als 0.95 ist.

### Aufgabe 4 (SS24)

Nina und Lea spielen ein Würfelspiel.

Gewonnen hat, wer als erster 5 Sechser gewürfelt hat.

#### Teilaufgabe A

Nina würfelt ihre 5te Sechs nach 16 Würfen. Wie groß ist dafür die Wahrscheinlichkeit?

Lösung über die Formel für die Binomialverteilung.

**ABER**: In die Formel müssen für n = 15 und k = 4 eingesetzt werden.

Warum? Weil es nur 15 Würfe sind, bei denen nicht klar ist, was das Ergebnis ist. Mit dem 16ten Wurf muss sie eine Sechs würfeln. Daher muss dieser Wurf extra behandelt werden.

n = 15
k = 4
p = 1/6
q = 5/6 (Gegenwahrscheinlichkeit)

P(X=15) = (n über k) * p hoch k * q hoch n - k

= (15 über 4) * 1/6 hoch 4 * 5/6 hoch 11

= 1365 * 0.000771605 * 0.134 

= 0.141

Die Wahrscheinlichkeit, dass mit dem 16. Wurf eine 6 erscheint, beträgt 1/6.

Beide Ereignisse werden gekoppelt:

P(X=16) = 0.0141 * 1/6 = 0.02362

Die Wahrscheinlichkeit beträgt ca. 23.62%

## ESA3

### Aufgabe 1: Restklassen Modulo

Berechnen Sie per Hand möglichst effizient

**a) 9 hoch 200 mod 23**

Anwenden des Eulerschen Theorem: 

9 φ(n) (1 mod n)

mit a = 9 und n = 23

Da n eine Primzahl ist, gilt φ(23) = 22

a und n sind teilerfremd, damit kann das Eulersches Theorem verwendet werden.

Der Exponent 200 wird mit der 22 zerlegt, in diesem Fall in 9 und Rest 2.

200 mod 22 = 200 - 9 * 22 = 200 - 198 = 2
=> 92 = 81 
=> 81 mod 23 = 12
=> 9 hoch 200 = 12 mod 23
=> 9 hoch 200 mod 23 = 12

**b) 9 hoch 200 mod 38**

Anwenden des Eulerschen Theorems mit a = 9 und n = 38

Beide sind teilerfremd, das Eulersche Theorem kann daher angewendet werden

φ(38) = 18

Vereinfachung durch Primfaktorenzerlegung von 38 in 2 und 19:

φ(2) = 1
φ(19) = 18
=> φ(38)  = 1 * 18

Reduktion des Exponenten

200 mod 18 = 200 − 11 x 18 = 200 − 198 = 2
=> 9 hoch 200 mod 38 = 92 mod 38
=> 9 hoch 2 = 81
=> 81 mod 38 = 81 − 2 x 38 = 81 − 76 = 5
=> 9 hoch 200 = 5 mod 38
=> 9 hoch 200 mod 38 = 5

### Aufgabe 2: Affine Chiffre

Eine monoalphabetische, monographische Chiffrierung eines deutschen Textes, der nur aus den 26 Großbuchstaben 
V = {A, B, C, D, E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X.Y, Z} besteht (Satzzeichen und Wortzwischenräume wurden weggelassen), funktioniere wie folgt:

Den Buchstaben aus V sind die Zahlen {0, 1, . . . , 25} zugeordnet, damit man mit ihnen rechnen kann.

(Wichtig: Die Zuordnung der Buchstaben zu Zahlen beginnt bei 0 und nicht bei 1)

a ➔ 0		b ➔ 1 		C ➔ 2		D ➔ 3 		E ➔ 4		F ➔ 5
G ➔ 6		H ➔ 7 		I ➔ 8		J ➔ 9 		K ➔ 10		L ➔ 11
M ➔ 12 	N ➔ 13 	O ➔ 14 	P ➔ 15		Q ➔ 16	R ➔ 17
S ➔ 18		T ➔ 19		U ➔ 20 	V ➔ 21		W ➔ 22 	X ➔ 23
Y ➔ 24 	Z ➔ 25

Die Verschlüsselung eines Klartextbuchstaben x → f (x) erfolgt über die Formel 

f(x) = (ax + b) mod 26 

mit ganzen Zahlen a, b ∈ {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 34,  25}

Sprachstatistische Auswertungen lassen den Schluss zu, dass durch die Verschlüsselung das G in S und das M in G übergeht.

Welche Werte haben a und b?

G ➔ 6		S ➔ 18	M ➔ 12

|Klartext|G ➔ 6|M ➔ 12|
|Geheimtext|S ➔ 18|G ➔ 6|

Aufstellen von zwei Gleichungen:

f(6) = (a * 6 + b) mod 26 = 18                // f(G) = S = (a*G + b) mod 26
f(12) = (a * 12 + b) mod 26 = 6			// f(M) = G = (a*M + b) mod 26

Auflösen des Gleichungssystems:

I 	6 a + b ≡ 18 		mod 26
II 	12 a + b ≡ 6 		mod 26

Subtraktion I - II, um b zu eliminieren:

III = I-II	(6a + b) - (12a +b) = 18 - 6		mod 26
-6a = 12				mod 26  
6a = -12 				mod 26

Addition von 26 auf der rechten Seite

da  -12 mod 26 = 14 => 6 a ≡ 14 mod 26

6 a  ≡ 14 mod 26

Für das Auflösen nach a wird das multiplikative Inverse von 6 mod 26 benötigt.

6 * x = 1 mod 26

Das gibt es aber nicht, da der ggT(6, 26) <> 1 ist. Es gibt es keine eindeutige Lösung.

a muss daher anders bestimmt werden, z.B. durch Ausprobieren Wir wandeln Gl. III in eine lineare Gleichung mit k ∈ ℤ um.

Es gilt 

6a = 14 + k · 26

Für welche k finden wir a in ℤ/26? 

Eine Iteration über k ergibt:

k=0:  	6a = 14 + k · 26
6a = 14 nicht gültig
k=1: 	6a = 14 + 26
6a = 40 nicht gültig
k=3: 	6a = 14 + 52
6a = 66
a1 = 11 ∈ ℤ/26
k=4: 	6a = 14 + 78
6a = 92 nicht gültig
k=5: 	6a = 14 + 104
6a = 118 nicht gültig
k=6: 	6a = 14 + 130
6a = 144
a2= 24 ∈ ℤ/26

Für a können demnach die Werte 11 und 24 eingesetzt werden.

a1 = 11 ∈ ℤ/26
a2 = 24 ∈ ℤ/26

Einsetzen von a in Gleichung I:

6*a1 + b1 ≡ 18 mod 26
6*a2 + b2 ≡ 18 mod 26
66 + b1 ≡ 18 mod 26
144 + b2 ≡ 18 mod 26
b1 ≡ -48 mod 26
b2 ≡ -126 mod 26				

NR: -48, -22, 4, 30
NR: -126, -100, …, -22, 4, 30

b1 ≡ 4 mod 26
b2 ≡ 4 mod 26			
b1 = 4 ∈ ℤ/26
b2 = 4 ∈ ℤ/26

Die Verschlüsselungsfunktionen lauten daher:

f(x) = (11x + 4) mod 26
f(x) = (24x + 4) mod 26
f(6) = 18 = a
f(6) = 18 = a
f(12) = 6 = b
f(12) = 6 = b

Probe:

???

## Aufgabe 3: RSA-Verschlüsselung

Zur Übermittlung geheimer Nachrichten verwenden Bob und Alice das RSA-Verfahren. Bob verfügt über die Primfaktorzerlegung von m = 55 und kann somit φ(m) berechnen. Als öffentlichen Schlüssel verwendet Bob e = 23

**a) Ist Bobs Schlüssel ein zulässiger Wert?**

Der Wert e = 23 muss die Anforderungen des RSA-Algorithmus erfüllen:

1. 1 < e < φ(m)

Als erstes wird die Eulersche Phi-Funktion verwendet.

m = 55 kann in die Primzahlen 5 und 11 zerlegt werden.

Damit wird φ(55) berechnet:

φ(m) = (p − 1) × (q − 1) = (5 − 1) × ( 11 − 1) = 4 × 10 = 40

Dies erste Bedingung ist erfüllt, denn 1 < 23 < 40 

2. Der größte gemeinsame Teiler (ggT) von e und φ(m) muss 1 sein

Durch Anwenden des Euklidischen Algorithmus ergibt sich ggT(23, 40) = 1

Der größte gemeinsame Teiler von e und φ(m) ist 1, deshalb ist die zweite Bedingung ebenfalls erfüllt.

Da beide Bedingungen erfüllt sind, ist e = 23 ein durchaus zulässiger Wert für Bob’s Schlüssel.

**b) Bob sendet diesen Schlüssel sofort an Alice, da sie ihm etwas sehr Wichtiges mitteilen will. Alice verschlüsselt ihre Klartextnachricht K (natürliche Zahl < 55) mit diesem Schlüssel. Es ergibt sich der Geheimtext C = 9. Wie lautet der Klartext?**

Bekannt sind:

- m = 55 für die Primfaktorzerlegung und Berechnen von phi(n)
- e für den öffentlichen Schlüssel = 23
- Geheimtext C = 9

Gesucht wird:

- Die Klartextnachricht K

Der Public Key ist (m, e) = (55, 23)

Primfaktorzerlegung von 55 in die Primzahlen 5 und 11

Berechnen der positiven ganzen Zahlen kleiner oder gleich m, die teilerfremd zu m sind, mit Hilfe der Eulerschen phi-Funktion:

φ(n) = (5-1) * (11-1) = 40

Für die Entschlüsselung wird die Variable d aus dem privaten Schlüssel berechnet.

Für d gilt die Bedingung e * d ≡ 1 (mod φ(n))

d muss als multiplikatives Invers von e mod φ(m) berechnet werden.

Dies geschieht entweder durch Ausprobieren oder über den erweiterten euklidischen Algorithmus mit a=23 und b=40.

40 = 1 * 23 + 17
23 = 1 * 17 + 6
17 = 2 * 6 + 5
6 = 1 * 5 + 1
5 = 5 * 1 + 0

Rückwärtseinsetzen für die Bestimmung des Inversen:

1 = 6 - 1 * 5
= 6 - 1 (17 - 2 * 6)
= 3 * 6 - 1 * 17
= 3 (23- 17) - 1 * 17
= 3 * 23 - 4 * 17
= 3 * 23 - 4 * (40 - 23)
= 7 * 23 - 4 * 40

Daraus folgt

7 * 23  ≡ 1 (mod 40)

und aus

7 * 23 - 4 * 40
d = 7

Mit d kann der Geheimtext über den privaten Schlüssel entschlüsselt werden:

K = 97 mod 55

9 hoch 7 mod 55 wird wie folgt berechnet:

9 hoch 2 = 81 ≡ 26 (mod 55)
9 hoch 4 = 262 = 676  ≡ 16 (mod 55)
9 hoch 7 = 9 * 96 = 9 * (94 * 92) = 9 * (16 * 26) (mod 55)

16 * 26 = 416  ≡ 31 (mod 55)
9 hoch 7 = 9 * 31 = 279  ≡ 4 (mod 55) = 4

Der Klartext ist damit 4