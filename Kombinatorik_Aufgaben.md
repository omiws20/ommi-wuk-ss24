# Kombinatorik-Aufgaben

## Aufgabe 1 (Klausur SS 2014)

Gegeben seien die Ziffern 1,1,2,2,3,3,3

**a) Wie viele verschiedene Anordnungen dieser Ziffern gibt es?**

Die klassische Permutation mit Multimengen

Anzahl = 7! / 3! * 2! * 2! = 210

**b) Wie viele dieser Ziffernfolgen beginnen mit einer 1 und enden mit einer 2?**

Anzahl = 5! / 3! = 20

**c) Wie viele dieser Ziffernfolgen gibt es, bei denen die beiden Einsen nebeneinander stehen?**

Anzahl = 6! / 2! * 3! = 60

## Aufgabe 2 (Klausur SS 2014)

Ein Student muss in einer Prüfung 10 von 13 Fragen beantworten.

**a) Auf wie viele verschiedene Weisen kann er 10 Fragen auswählen (und beantworten)?**

Das ist Grundaufgabe 3 mit n = 13 und k = 10

Anzahl = (13 über 10) = 286

**b) Auf wie viele Weisen, falls er auf jeden Fall die beiden ersten Fragen beantworten muss?**

Es gibt nur noch 11 Fragen und er wählt 8 Fragen aus.

Anzahl = (11 über 8) = 165

**c) Auf wie viele Weisen, falls er **genau** 3 der ersten 5 Fragen beantworten muss?**

**Schritt 1**: 3 aus 5

Anzahl1 = (5 über 3) = 10

**Schritt 2**: 8 über 7

Anzahl2 = (8 über 7) = 8

Anzahl = 10 * 8 = 80

**d) Auf wie viele Weisen, falls er **mindestens** 3 der ersten 5 Fragen beantworten muss?**

Jetzt wird es etwas "komplizierter", da drei Multimengen-Permutationen kombiniert werden müssen.

Anzahl = (5 über 3) * (8 über 7) + (5 über 4) * (8 über 6) + (5 über 5) * (8 über 5) = 276

## Aufgabe 3 (Klausur SS 2014)

In einem durchsichtigen Sack befinden sich 2 rote, 2 blaue, 2 gelbe und 2 weiße Kugeln. Jemand greift blind hinein und holt gleichzeitig 3 Kugeln heraus.

Wie groß ist die Wahrscheinlichkeit dafür, dass

**a) die gezogenen Bälle alle verschiedenfarbig sind?**

Es sind insgesamt 8 Kugeln. Es gibt damit immer 8 über 3 = 56 Kombinationsmöglichkeiten.

Es sind 4 Farben, die 8 Mal kombiniert werden können.

Anzahl = 4 * 8 / (8 über 3) = 0.571 = 57.1%

**b) Genau zwei gleichfarbige Bälle und ein andersfarbiger Ball gezogen wird?**

*** warum 4 * 6?

Anzahl = 4 * 6 / (8 über 3) = 0.428 = 42.8%

**c) Mindestens ein roter Ball dabei ist?**

Das wird am einfachsten über die Gegenwahrscheinlichkeit berechnet.

Zuerst die Wahrscheinlichkeit, dass kein roter Ball dabei ist, wenn 3 Bälle gezogen werden.

PkeinRot = (6 über 3) / (8 über 3) = 0.357

PeinRot = 1 - 0.357 = 0.642 = 64.2%

## Aufgabe 4 (Klausur SS 2014)

Riana wirft zwei faire Würfel.

**a) Welche der möglichen Augenzahlen ∈ {2,3,4,...,11,12} erscheint mit der größten Wahrscheinlichkeit und wie groß ist diese?**

Die 7 erscheint mit einer Wahrscheinlichkeit von 6/36 = 1/6.

**b) Mit welcher Wahrscheinlichkeit zeigt mindestens einer der Würfel eine 1 oder eine 2?**

*** warum Gegenwahrscheinlichkeit und warum 16?

W1oder2 = 1 - 16/36 = 5/9

ChatGPT sagt dazu:

Die Wahrscheinlichkeit, dass ein einzelner Würfel keine 1 oder 2 zeigt (also eine der Zahlen 3, 4, 5 oder 6 zeigt), beträgt:

P(kein 1 oder 2) = 4/6 = 2/3

Da die beiden Würfel unabhängig voneinander sind, ist die Wahrscheinlichkeit, dass beide Würfel keine 1 oder 2 zeigen:

P(kein 1 oder 2 bei beiden Würfeln) = 2/3 * 2/3 = 4/9

Die Gegenwahrscheinlichkeit ist die Wahrscheinlichkeit, dass mindestens einer der beiden Würfel eine 1 oder eine 2 zeigt:

P(1 oder 2 bei beiden Würfeln) = 1 - 4/9 = 5/9

Damit ist 16/36 eine andere Schreibweise für 4/9 und damit stimmt es. Nur wie kam der Dozent von Anfang an auf 16?

## Aufgabe 4 (Skript, Seite 87)

Ein Eisladen in Ettlingen bietet 10 Eissorten an.

Wie viele Zusammenstellungen gibt es für zwei Eiswaffeln mit je 4 verschiedenen Kugeln, so dass alle 8 Eissorten unterschiedlich sind?

Das Vertauschen der Waffeln soll keiner neuen Verteilung entsprechen.

### Lösung

**Schritt 1**: Es gibt 10 über 8 Möglichkeiten, 8 Eissorten auszuwählen

Anzahl = (10 über 8) = 10! / (8! * 2!) = 45

**Schritt 2**: Es gibt 8 über 4 Möglichkeiten, diese auf zwei Waffeln mit je 4 Kugeln zu verteilen

Anzahl = (10 über 8) * (8 über 4) / 2 = 1575 Paare

Das Teilen durch 2 ist erforderlich, da die beiden Eiswaffeln nicht unterschieden werden. 

## Aufgabe 5 (Skript, Seite 88)

Ein Eisladen in Esslingen bietet 10 Eissorten an.

Wie viele verschiedene Zusammenstellungen von Eissorten gibt es für zwei Eiswaffeln mit je 4 verschiedenen Kugeln, wobei die beiden Waffeln auch gemeinsame Sorten haben dürfen.

Das Vertauschen der beiden Waffeln soll keiner neuen Verteilung entsprechen.

### Lösung

Die Menge M der Eiswaffeln mit 4 verschiedenen Sorten ist

Anzahl = (10 über 4) = 210

So viele Kombinationen lassen sich für eine Waffel bilden.

Jetzt sollen zwei Waffeln kombiniert werden. Auch wenn es naheliegend wäre, einfach 210 * 210 zu rechnen, ist das nicht die Lösung.

Es muss vielmehr die Anzahl der 2-Elementigen-Teil-Multimengen der Gesamtmenge M = 210 berechnet werden.

Das ist Grundaufgabe 4 (Ohne Reihenfolge mit Zurücklegen) mit n = 210 und k = 2

Anzahl = (210 + 1 - 2) / 2 = 22155

Nach Grundaufgabe 4 besitzt eine n-elementige Menge (n + k − 1/  k verschiedene k-elementige Teil-Multimengen


## Aufgabe 6 (Skript, Seite 77)

Jemand will in einem Saftladen 6 Saftflaschen kaufen, es stehen drei Sorten zur Auswahl, die beliebig kombiniert werden dürfen..

Wie viele Möglichkeiten gibt es, einen solchen Kasten zusammenzustellen?

### Lösung

Das ist Grundaufgabe 4 (Ungeordnet mit Zurücklegen) mit n = 3 und k = 6

Anzahl = (3 + 6 - 1 über 6) = 28 

*ChatGPT hat die Lösung richtig gerechnet*bb

## Aufgabe 7
Ein Geschäft erhält eine Lieferug von 30 PCs, von denen 6 defekt sind. 4 PCs werden an ein Altenheim geliefert.

**a) Wie viele Zusammenstellungen von PCs gibt es insgesamt?**
**b) Wie viele Zusammenstellungen werden keinen defekten PC enthalten?**

### Lösungen

zu a)

Das ist Grundaufgabe 3 mit n = 30 und k = 4

Anzahl = (30 über 4) = 27405

zu b)

Das ist Grundaufgabe 3 mit n = 24 und k = 6

Anzahl = (24 über 4) = 10626