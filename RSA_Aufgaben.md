# RSA-Aufgaben

## Aufgabe 1 (ChatGPT)

Verwende die RSA-Verschlüsselung, um die Nachricht "HI" (in numerischer Form) zu verschlüsseln und dann zu entschlüsseln.

**Schritt 1**: Wähle zwei Primzahlen aus

p = 5
q = 11

**Schritt 2**: Berechne *n* und *ϕ(n)* 

n = p ⋅ q

ϕ(n) = (p−1) ⋅ (q−1)

n = 55

ϕ(n) = 40

**Schritt 3**: Wähle einen öffentlichen Exponenten *e*

Wähle *e*, so dass *1 < e < ϕ(n)* und *e* teilerfremd zu *ϕ(n)* erfüllt sind (gemeinsame Teiler sind nur 1).

e = 3

*e* muss nicht immer 23 sein;)

Vorausgesetzt gcd(3,40) = 1

**Schritt 4**: Wähle einen öffentlichen Exponenten *e*

*d* sollte so gewählt werden, dass gilt:

d ⋅ e mod ϕ(n) = ϕ(n) = 1

*d* muss entweder über den inversen Euklid ausgerechnet oder durch Ausprobieren herausgefunden werden:

Berechne *d* so, dass *d ⋅ 3 mod 40 = 1* ist. Eine mögliche Lösung wäre d = 27.

**Schritt 5:** Verschlüsselung der Nachricht

Konvertiere die Nachricht "HI" in eine numerische Form. Verwende beispielsweise ASCII-Werte:

H = 72, I = 73

Eine Möglichkeit, dies zu kodieren, ist 7273.

Verschlüssle die Nachricht *m* mit dem öffentlichen Schlüssel *(e,n): c hoch d mod n*

c = 7273 hoch 3 mod 55
m = c hoch 27 mod 55

**Schritt 6:** Entschlüsselung der Nachricht

** fehlt noch

## Aufgabe 2 (ESA SS 24)

Zur Übermittlung geheimer Nachrichten verwenden Bob und Alice das RSA-Verfahren. Bob verfügt über die Primfaktorzerlegung von m = 55 und kann somit φ(m) berechnen. Als öffentlichen Schlüssel verwendet Bob e = 23

**a) Ist Bobs Schlüssel ein zulässiger Wert?**

### Lösung

Der Wert e = 23 muss die Anforderungen des RSA-Algorithmus erfüllen:

1 < e < φ(m)

Als erstes wird die Eulersche Phi-Funktion verwendet.

m = 55 kann in die Primzahlen 5 und 11 zerlegt werden.

Damit wird φ(55) berechnet:

φ(m) = (p − 1) × (q − 1) = (5 − 1) × ( 11 − 1) = 4 × 10 = 40

Dies erste Bedingung ist erfüllt, denn 1 < 23 < 40

Der größte gemeinsame Teiler (ggT) von e und φ(m) muss 1 sein

Durch Anwenden des Euklidischen Algorithmus ergibt sich ggT(23, 40) = 1

Der größte gemeinsame Teiler von e und φ(m) ist 1, deshalb ist die zweite Bedingung ebenfalls erfüllt.

Da beide Bedingungen erfüllt sind, ist e = 23 ein durchaus zulässiger Wert für Bob’s Schlüssel.

b) Bob sendet diesen Schlüssel sofort an Alice, da sie ihm etwas sehr Wichtiges mitteilen will. Alice verschlüsselt ihre Klartextnachricht K (natürliche Zahl < 55) mit diesem Schlüssel. Es ergibt sich der Geheimtext C = 9.

Wie lautet der Klartext?

### Lösung

Bekannt sind:

m = 55 für die Primfaktorzerlegung und Berechnen von phi(n)
e für den öffentlichen Schlüssel = 23
Geheimtext C = 9

Gesucht wird:

Die Klartextnachricht K

Der Public Key ist (m, e) = (55, 23)

Primfaktorzerlegung von 55 in die Primzahlen 5 und 11

Berechnen der positiven ganzen Zahlen kleiner oder gleich m, die teilerfremd zu m sind, mit Hilfe der Eulerschen phi-Funktion:

φ(n) = (5-1) * (11-1) = 40

Für die Entschlüsselung wird die Variable d aus dem privaten Schlüssel berechnet.

Für d gilt die Bedingung e * d ≡ 1 (mod φ(n))

d muss als multiplikatives Invers von e mod φ(m) berechnet werden.

Dies geschieht entweder durch Ausprobieren oder über den erweiterten euklidischen Algorithmus mit a=23 und b=40.

40 = 1 * 23 + 17
23 = 1 * 17 + 6
17 = 2 * 6 + 5
6 = 1 * 5 + 1
5 = 5 * 1 + 0

Rückwärtseinsetzen für die Bestimmung des Inversen:

1 = 6 - 1 * 5
= 6 - 1 (17 - 2 * 6)
= 3 * 6 - 1 * 17
= 3 (23- 17) - 1 * 17
= 3 * 23 - 4 * 17
= 3 * 23 - 4 * (40 - 23)
= 7 * 23 - 4 * 40

Daraus folgt

7 * 23  ≡ 1 (mod 40)

und aus

7 * 23 - 4 * 40
d = 7

Mit d kann der Geheimtext über den privaten Schlüssel entschlüsselt werden:

K = 97 mod 55

9 hoch 7 mod 55 wird wie folgt berechnet:

9 hoch 2 = 81 ≡ 26 (mod 55)
9 hoch 4 = 262 = 676  ≡ 16 (mod 55)
9 hoch 7 = 9 * 96 = 9 * (94 * 92) = 9 * (16 * 26) (mod 55)
16 * 26 = 416  ≡ 31 (mod 55)
9 hoch 7 = 9 * 31 = 279  ≡ 4 (mod 55) = 4

Der Klartext ist damit 4

## Aufgabe 3 (Klausur SS 2014)

In einem Public Key-System nach dem RSA-Verfahren fangen Sie als blöder Lauscher die verschlüsselte Nachricht C=10 ab, das an den Empfänger gerichtet ist, dessen Public Key aus e=5 und m=35 (Modul) besteht.

Wie lautet die Klartextnachricht Z?

### Lösung

Das Modul *m* kann in die Primzahlen 5 und 7 verlegt werden, daher beträgt der "Hilfsmodul" (in anderen Lösungen phi) 

m* = 4*6 = 24

Für den privaten Schlüssel gilt

d = e hoch -1 mod 24
  = 5 hoch -1 mod 24
  = 5

Mit *d* lässt sich die Nachricht *C* problemlos entschlüsseln:

z = c hoch d (mod 35)
 = 10 hoch 5 (mod 35)
 = 5

 *** Wenn es so klar ist, dauert das Hinschreiben keine 5 Minuten;)

